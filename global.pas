unit Global;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

var
  GLOBAL_DATA: TDateTime;
  GLOBAL_DATA_BASE: TDateTime;
  GLOBAL_DATA_COMPARATIVO: String = '';

const
  Site: String  = 'www.glicbox.com.br';
  Perfil_Acesso: String  = 'O';
  VersaoAtual  : Integer = 1907060;
  sVersaoAtual : String  = '19.07.06s0';
  Versao_script: Integer = 0;
  ID_GLIC      : Integer = 0;

  GLOBAL_AMBIENTE_TESTE: Boolean = False;
  GLOBAL_EMPRESA_PADRAO: Integer = 0;

  GLOBAL_ULTIMO_BACKUP : TDateTime = 0;


implementation

uses funcoes;

end.

