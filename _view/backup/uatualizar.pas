unit uatualizar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IdHTTP, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, ComCtrls, DBGrids, ufmodelo, IdComponent, IdAntiFreeze,
  zipper, IniFiles, db;

type

  { TFAtualizar }

  TFAtualizar = class(TFModelo)
    btAtualizar: TButton;
    btCancelar: TButton;
    dsScript: TDataSource;
    DBGrid1: TDBGrid;
    IdHTTP: TIdHTTP;
    lblStatus: TLabel;
    lbPorcentagem: TLabel;
    Memo1: TMemo;
    pbprogresso: TProgressBar;
    procedure btAtualizarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCount: Int64);
    procedure IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
      AWorkCountMax: Int64);
    procedure IdHTTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
  private
     function RetornaPorcentagem(ValorMaximo, ValorAtual: real): string;
     function RetornaKiloBytes(ValorAtual: real): string;
     function ExtrairZip(sArquivo: String): Boolean;
     function baixarIniVersao: Boolean;
     function baixarUltimaVersao: Boolean;
     function AtualizaScript:Boolean;
     function ExecutaAtualizadorExe:Boolean;
     function VerificaLibMySql:Boolean;
     function VerificaAtualizador: Boolean;
     function VerificaIniAtualizador: Boolean;
  public
     function VerificarVersao:Boolean;
  end;

var
  FAtualizar: TFAtualizar;

implementation

uses Global, funcoes, udmPrincipal, Glic.Mensagem, Glic.Acesso;

{$R *.lfm}

{ TFAtualizar }

procedure TFAtualizar.IdHTTPWorkBegin(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCountMax: Int64);
begin
  pbprogresso.Max      := AWorkCountMax;
  pbprogresso.Position := 0;
end;

procedure TFAtualizar.IdHTTPWorkEnd(ASender: TObject; AWorkMode: TWorkMode);
begin
  lbPorcentagem.Caption := 'Finalizado ...';
  lblStatus.Caption     := 'Download Finalizado ...';
  Application.ProcessMessages;
end;

function TFAtualizar.RetornaPorcentagem(ValorMaximo, ValorAtual: real): string;
var
  resultado: Real;
begin
  try
    if (ValorMaximo > 0) and (ValorAtual > 0) then
    begin
      resultado := ((ValorAtual * 100) / ValorMaximo);
      Result    := FormatFloat('0%', resultado);
    end;
  except
  end;
end;

function TFAtualizar.RetornaKiloBytes(ValorAtual: real): string;
var
  resultado : real;
begin
  try
    if (ValorAtual > 0) then
    begin
      resultado := ((ValorAtual / 1024) / 1024);
      Result    := FormatFloat('0.000 KBs', resultado);
    end;
  except
  end;
end;

function TFAtualizar.ExtrairZip(sArquivo: String): Boolean;
var
  UnZipper: TUnZipper;
begin
  Result   := False;
  try
    UnZipper := TUnZipper.Create;
    try
      UnZipper.FileName   := sArquivo;
      UnZipper.OutputPath := ExtractFilePath(sArquivo);
      UnZipper.Examine;
      UnZipper.UnZipAllFiles;
      Result := True;
    finally
      UnZipper.Free;
    end;
  except
    Result := False;
  end;
end;

function TFAtualizar.baixarIniVersao: Boolean;
var
  fileDownload : TFileStream;
  sArquivo: String;
begin
  lbPorcentagem.Caption := '';
  lblStatus.Caption     := '';
  pbprogresso.Position  := 0;
  Result := False;
  if not(DirectoryExists(ExtractFilePath(Application.ExeName)+'db\')) then
    ForceDirectories(ExtractFilePath(Application.ExeName)+'db\');

  sArquivo := ExtractFilePath(Application.ExeName)+'db\versao.zip';

  DeleteFile(sArquivo);

  try
    fileDownload := TFileStream.Create(sArquivo, fmCreate);
    try
      if GLOBAL_AMBIENTE_TESTE then
        IdHTTP.Get('http://engrer.com/download/teste/Versao.zip', fileDownload)
      else
        IdHTTP.Get('http://engrer.com/download/Versao.zip', fileDownload);
    finally
      FreeAndNil(fileDownload);
    end;
  except
    Result := False;
    Exit;
  end;
  if FileExists(sArquivo) then
  begin
    if ExtrairZip(sArquivo) then
    begin
      if FileExists(ExtractFilePath(sArquivo)+'Versao.ini') then
      begin
        DeleteFile(sArquivo);
        Result := True;
      end;
    end;
  end;
end;

function TFAtualizar.baixarUltimaVersao: Boolean;
var
  fileDownload : TFileStream;
  sArquivo: String;
begin
  lbPorcentagem.Caption := '';
  lblStatus.Caption     := '';
  pbprogresso.Position  := 0;
  Result := False;
  if not(DirectoryExists(ExtractFilePath(Application.ExeName)+'db\')) then
    ForceDirectories(ExtractFilePath(Application.ExeName)+'db\');

  sArquivo := ExtractFilePath(Application.ExeName)+'db\GlicTransportes.zip';

  DeleteFile(sArquivo);

  try
    fileDownload := TFileStream.Create(sArquivo, fmCreate);
    try
      if GLOBAL_AMBIENTE_TESTE then
        IdHTTP.Get('http://engrer.com/download/teste/GlicTransportes.zip', fileDownload)
      else
        IdHTTP.Get('http://engrer.com/download/GlicTransportes.zip', fileDownload);
    finally
      FreeAndNil(fileDownload);
    end;
  except
    Result := False;
    Exit;
  end;

  if FileExists(sArquivo) then
  begin
    if ExtrairZip(sArquivo) then
    begin
      if FileExists(ExtractFilePath(sArquivo)+'GlicTransportes.exe') then
      begin
        DeleteFile(sArquivo);
        Result := True;
      end;
    end;
  end;
end;

function TFAtualizar.AtualizaScript: Boolean;
var
  sLog : TStringList;
  sVersao: String;
begin
  try
    sLog := TStringList.Create;
    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text := 'SELECT versao_script FROM configuracoes LIMIT 1';
    dmPrincipal.qryTemp.Open;

    Versao_script := dmPrincipal.qryTemp.Fields[0].AsInteger;

    if not VerificaLibMySql then
    begin
      sLog.Add('Sem libMysql.dll');
      sLog.SaveToFile(ExtractFilePath(Application.ExeName)+'Log.txt');
      TGlicMensagem.GetInstance.Show('Não foi possivel baixar scripts para Atualização',GlicErro);
      Result := False;
      Exit;
    end;
    try
      dmPrincipal.Conexao.StartTransaction;
      dmPrincipal.qryScript.Close;
      dmPrincipal.qryScript.SQL.Add(' WHERE script.versao > '+Versao_script.ToString()+' order by script.versao');
      dmPrincipal.qryScript.Open;
      dmPrincipal.qryScript.First;
      while not dmPrincipal.qryScript.EOF do
      begin
        try
          Memo1.Clear;
          sVersao    := dmPrincipal.qryScript.Fields[1].AsString;
          Memo1.Text := dmPrincipal.qryScript.Fields[2].AsString;
        //  sLog.Text := Memo1.Text;
          dmPrincipal.RodarScript.Clear;
          dmPrincipal.RodarScript.Script.Text := Memo1.Text;
          dmPrincipal.RodarScript.Execute;
        finally
          dmPrincipal.qryScript.Next;
        end;
      end;
      dmPrincipal.Conexao.Commit;
      Result := True;
    except
      dmPrincipal.Conexao.Rollback;
      sLog.Add('Erro ao rodar script => '+sVersao);
      sLog.SaveToFile(ExtractFilePath(Application.ExeName)+'Log.txt');
      DeleteFile(ExtractFilePath(Application.ExeName)+'lib\libmysql.dll');
      if GLOBAL_AMBIENTE_TESTE then
        TGlicMensagem.GetInstance.Show('Erro ao rodar script no banco de dados!'+#13+'Script: '+sVersao,GlicErro);
      Result := False;
    end;
  finally
    dmPrincipal.qryScript.Close;
    dmPrincipal.ConexaoMysql.Connected := False;
    FreeAndNil(sLog);
  end;
end;

function TFAtualizar.ExecutaAtualizadorExe: Boolean;
begin
  if not VerificaAtualizador then
  begin
    Result := False;
    Exit;
  end;

  if not VerificaIniAtualizador then
  begin
    Result := False;
    Exit;
  end;

  if ExecutarApp('Atualizador.exe') then
    Result := True
  else
    Result := False;
end;

function TFAtualizar.VerificaLibMySql: Boolean;
var
  fileDownload : TFileStream;
  sArquivo : String;
begin
  try
    try
      sArquivo := ExtractFilePath(Application.ExeName)+'lib\libmysql.dll';
      if FileExists(sArquivo) then
      begin
        Result := True;
      end
      else
      begin
        try
          fileDownload := TFileStream.Create(sArquivo, fmCreate);
          try
            IdHTTP.Get('http://engrer.com/download/libmysql.dll', fileDownload);
          finally
            FreeAndNil(fileDownload);
          end;
          if FileExists(sArquivo) then
            Result := True;
        except
          Result := False;
          Exit;
        end;
      end;
    except
      Result := False;
    end;
  finally
  end;
end;

function TFAtualizar.VerificaAtualizador: Boolean;
var
  sArquivo : String;
  fileDownload : TFileStream;
begin
  try
    try
      sArquivo := 'C:\lazprojeto\GlicTransporte\Atualizador.exe';
      if FileExists(sArquivo) then
      begin
        Result := True;
      end
      else
      begin
        lbPorcentagem.Caption := '';
        lblStatus.Caption     := '';
        pbprogresso.Position  := 0;
        try
          fileDownload := TFileStream.Create('C:\lazprojeto\GlicTransporte\db\Atualizador.zip', fmCreate);
          try
            IdHTTP.Get('http://engrer.com/download/Atualizador.zip', fileDownload);
          finally
            FreeAndNil(fileDownload);
          end;
        except
          Result := False;
          Exit;
        end;

        if FileExists('C:\lazprojeto\GlicTransporte\db\Atualizador.zip') then
          if ExtrairZip('C:\lazprojeto\GlicTransporte\db\Atualizador.zip') then
            DeleteFile('C:\lazprojeto\GlicTransporte\db\Atualizador.zip');

        CopyFile('C:\lazprojeto\GlicTransporte\db\Atualizador.exe','C:\lazprojeto\GlicTransporte\Atualizador.exe',False);
        if FileExists(sArquivo) then
          Result := True
        else
          Result := False;
      end;
    except
      Result := False;
    end;
  finally
  end;
end;

function TFAtualizar.VerificaIniAtualizador: Boolean;
var
  ini : TIniFile;
  sArquivo : String;
begin
  try
    try
      sArquivo := 'C:\lazprojeto\GlicTransporte\Atualizador.ini';
      DeleteFile(sArquivo);
      if FileExists(sArquivo) then
      begin
        Result := False;
      end
      else
      begin
        try
          ini := TIniFile.Create(sArquivo);
          ini.WriteString('GlicTransportes.exe','ORIGEM','C:\lazprojeto\GlicTransporte\db');
          ini.WriteString('GlicTransportes.exe','DESTINO','C:\lazprojeto\GlicTransporte');
          ini.WriteString('GlicTransportes.exe','EXECUTAR','S');
          result := True;
        finally
          FreeAndNil(ini);
        end;
      end;
    except
      Result := False;
    end;
  finally
  end;
end;

function TFAtualizar.VerificarVersao: Boolean;
var
  ini: TIniFile;
  sArquivo : String;
  iUltimaVersao: Integer;
begin
  try
    if not(baixarIniVersao) then
    begin
      Result := False;
      Exit;
    end;
    iUltimaVersao := 0;
    Result        := False;
    sArquivo      := ExtractFilePath(Application.ExeName)+'db\Versao.ini';
    if not(FileExists(sArquivo)) then
    begin
      Result := False;
      Exit;
    end;
    try
      ini := TIniFile.Create(sArquivo);
      iUltimaVersao := StrToInt(ini.ReadString('VERSAO','EXE','0'));
    finally
      FreeAndNil(ini);
    end;
    if iUltimaVersao > VersaoAtual then
    begin
      Result := True;
    end
    else
    begin
      Result := False;
    end;
  except
    Result := False;
  end;
end;

procedure TFAtualizar.IdHTTPWork(ASender: TObject; AWorkMode: TWorkMode;
  AWorkCount: Int64);
begin
  pbprogresso.Position  := AWorkCount;
  lblStatus.Caption     := 'Baixando ... ' + RetornaKiloBytes(AWorkCount);
  lbPorcentagem.Caption := 'Download em ... ' + RetornaPorcentagem(pbprogresso.Max, AWorkCount);
  Application.ProcessMessages;
end;

procedure TFAtualizar.btAtualizarClick(Sender: TObject);
begin
  try
    btAtualizar.Enabled := false;
    pbprogresso.Visible := True;
    lblStatus.Visible   := True;
    if VerificarVersao then
    begin
      if not(baixarUltimaVersao) then
      begin
        TGlicMensagem.GetInstance.Show('Não foi possivel baixar Atualização',GlicErro);
      end
      else
      begin
        if AtualizaScript then
        begin
          if ExecutaAtualizadorExe then
          begin
            TGlicAcesso.GetInstance.finalizar;
          end;
        end;
      end;
    end
    else
    begin
      if Self.Showing then;
        TGlicMensagem.GetInstance.Show('Sistema já se encontra atualizado!',GlicAviso);
    end;
  finally
    btAtualizar.Enabled := True;
  end;
end;

procedure TFAtualizar.btCancelarClick(Sender: TObject);
begin
  IdHTTP.Disconnect;
  btAtualizar.Enabled := True;
end;

procedure TFAtualizar.btnCloseClick(Sender: TObject);
begin
  inherited;
end;

end.

