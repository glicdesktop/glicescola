unit uBusca;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls, DBGrids, DbCtrls, ufmodelo, udmPrincipal, Global, funcoes, db;

type

  { TFBusca }

  TFBusca = class(TFModelo)
    btnBusca: TButton;
    dsBusca: TDataSource;
    edBusca: TEdit;
    Grid: TDBGrid;
    Label1: TLabel;
    lbTotal: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    procedure btnBuscaClick(Sender: TObject);
    procedure dsBuscaDataChange(Sender: TObject; Field: TField);
    procedure edBuscaChange(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure GridKeyPress(Sender: TObject; var Key: char);
  private
    sql: String;
  public
    Buscando: TBuscGlic;
    id: String;
  end;

var
  FBusca: TFBusca;

implementation

{$R *.lfm}

{ TFBusca }

procedure TFBusca.dsBuscaDataChange(Sender: TObject; Field: TField);
begin
 lbTotal.Caption := dsBusca.DataSet.RecordCount.ToString;
end;

procedure TFBusca.edBuscaChange(Sender: TObject);
begin
  btnBusca.OnClick(nil);
end;

procedure TFBusca.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
  if Key = 13 then
  begin
    Grid.OnDblClick(nil);
  end;
  if Key = 38 then //Para cima
    dmPrincipal.qryBusca.Prior;
  if Key = 40 then //Para Baixo
    dmPrincipal.qryBusca.Next;
end;

procedure TFBusca.FormKeyPress(Sender: TObject; var Key: char);
begin
  inherited;
end;

procedure TFBusca.FormShow(Sender: TObject);
begin
  grid.Columns[2].Visible := False;
  case Buscando of
  cidade :
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome, '+
             '    busca.ud as sigla '+
             ' from '+
             '    cidade as busca ';
      grid.Columns[2].Visible := True;
    end;
  estado:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome, '+
             '    busca.sigla '+
             ' from '+
             '    estado as busca';
      grid.Columns[2].Visible := True;
    end;
  cliente:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.fantasia as nome, '+
             '    busca.destino  as sigla '+
             ' from '+
             '    cliente as busca';
      grid.Columns[2].Visible := True;
      grid.Columns[0].Width   := 50;
      grid.Columns[1].Width   := 280;
      grid.Columns[2].Width   := 280;
      grid.Columns[2].Title.Caption := 'Destino';
    end;
  fornecedor:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome_fantasia as nome, '+
             '    '''' as sigla '+
             ' from '+
             '    fornecedor as busca';
    end;
  motorista:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome, '+
             '    '''' as sigla '+
             ' from '+
             '    motorista as busca';
    end;
  empresa:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome, '+
             '    '''' as sigla '+
             ' from '+
             '    empresa as busca';
    end;
  conta_gerencial:
    begin
      sql := ' select  '+
             '    busca.id, '+
             '    busca.nome, '+
             '    '''' as sigla '+
             ' from '+
             '    conta_gerencial as busca';
    end;
  Veiculo :
    begin
      sql := 'select '+
             '    id,'+
             '    placa as nome,'+
             '    marca as sigla'+
             ' from veiculo as busca';
      grid.Columns[1].Title.Caption := 'Placa';
      grid.Columns[1].Width         := 130;
      grid.Columns[2].Title.Caption := 'Marca';
      grid.Columns[2].Visible       := True;
      grid.Columns[2].Width         := 300;
    end;
  end;
  btnBusca.OnClick(nil);
end;

procedure TFBusca.GridDblClick(Sender: TObject);
begin
  if not dmPrincipal.qryBusca.IsEmpty then
  begin
    case Buscando of
      Veiculo: Self.id := dmPrincipal.qryBuscanome.AsString;
    else
      Self.id := dmPrincipal.qryBuscaid.AsString;
    end;
  end
  else
    Self.id := '';
  Close;
end;

procedure TFBusca.GridKeyPress(Sender: TObject; var Key: char);
begin
  edBusca.SetFocus;
end;

procedure TFBusca.btnBuscaClick(Sender: TObject);
var
  sWhere: String;
begin
  dmPrincipal.qryBusca.Close;
  case Buscando of
    cliente    : sWhere := ' WHERE busca.fantasia like '+QuotedStr(edBusca.Text+'%');
    fornecedor : sWhere := ' WHERE busca.nome_fantasia like '+QuotedStr(edBusca.Text+'%');
    Veiculo    : sWhere := ' WHERE busca.placa like '+QuotedStr(edBusca.Text+'%');
  else
    sWhere := ' WHERE busca.nome like '+QuotedStr(edBusca.Text+'%');
  end;
  dmPrincipal.qryBusca.SQL.Text := sql + sWhere;
  dmPrincipal.qryBusca.Open;
end;

end.

