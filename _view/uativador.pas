unit uAtivador;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, IdHTTP, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons, ufmodelo, IniFiles, zipper;

type

  { TFAtivador }

  TFAtivador = class(TFModelo)
    btAtivar: TBitBtn;
    edChave: TEdit;
    IdHTTP: TIdHTTP;
    lbSerial: TEdit;
    Label1: TLabel;
    lbValidade: TLabel;
    lbValidade1: TLabel;
    procedure btAtivarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    var
    var
      sNomeArquivo: String;
  public
    function baixarIniSerial: Boolean;
    function ChecarLiberacao: Boolean;
    var
      sHora : String;
      bLiberado : Boolean;
  end;

var
  FAtivador: TFAtivador;

implementation

uses udmPrincipal, Global, funcoes, Criptografia_app, Mensagem_Ctrl;

{$R *.lfm}

{ TFAtivador }

procedure TFAtivador.btAtivarClick(Sender: TObject);
var
  dNovaData: TDate;
  sBaseAtualizada: String;
  encrypta : String;
  sLog : TStringList;
  Chave,Resultado : String;
  texto : String[70];
  I     : Integer;
begin
  try
    if not(Trim(edChave.Text) <> '') or not(Length(Trim(edChave.Text)) = 23) then
    begin
      TMensagem.GetInstance.Show('Informe uma chave válida!', Erro_app);
      Exit;
    end;
    Chave := '';
    texto := edChave.Text;
    for I := 0 to Length(edChave.Text) do
    begin
      if texto[I] in ['A'..'Z','0'..'9'] then
        Chave := Chave + texto[I];
    end;
    try
      if not(Trim(Chave) <> '') or not(Trim(sHora) <> '') then
      begin
        TMensagem.GetInstance.Show('Chave Inválido!', Erro_app);
        Exit;
      end;

      encrypta         := FormatDateTime('dd/mm/yyyy',Date);
      encrypta         := TextToCriptoHex(encrypta);
      sBaseAtualizada  := copy(encrypta, 1, 5)+
                          copy(encrypta, 6, 5)+
                          copy(encrypta, 11, 5)+
                          copy(encrypta, 16, 5);
      encrypta      := CriptoHexToText(Chave, sHora);
      dNovaData     := StrToDate(encrypta);
      encrypta      := DateToStr(dNovaData);
      if (Length(TextToCriptoHex(encrypta)) = 20) and (Length(sBaseAtualizada) = 20) then
      begin
        try
          dmPrincipal.qryTemp.Close;
          dmPrincipal.qryTemp.SQL.Text   := ' UPDATE configuracoes SET ' +
                                            ' COMPARATIVO = ' + QuotedStr(TextToCriptoHex(encrypta)) + ', ' +
                                            ' BASE = ' + QuotedStr(sBaseAtualizada);
          dmPrincipal.qryTemp.ExecSQL;
        finally
        end;
      end
      else
      begin
        TMensagem.GetInstance.Show('Chave Inválido!', Erro_app);
        Exit;
      end;
      TMensagem.GetInstance.Show('Sistema Liberado!', Erro_app);
      bLiberado := True;
      ExecutarApp(Application.ExeName);
      Halt(0);
    except
      on E: Exception do
      begin
        sLog := TStringList.Create;
        sLog.Add('Erro ao tentar liberar o sistema: ' + FormatDateTime('dd/mm/yyyy hh:mm',Now) + '  -  ' + E.Message);
        sLog.SaveToFile(ExtractFilePath(Application.ExeName)+'Log.txt');
      end;
    end;
  finally
  end;
end;

procedure TFAtivador.FormCreate(Sender: TObject);
var
  encrypta : string;
begin
  inherited;
  sNomeArquivo := 'serial_escola';
  if Trim(Global.GLOBAL_DATA_COMPARATIVO) <> '' then
    lbValidade.Caption := 'Sistema válido até: ' + (Global.GLOBAL_DATA_COMPARATIVO)
  else
    lbValidade.Caption := '';

  sHora            := FormatDateTime('hhmm', Time);
  encrypta         := '2H*' + sHora + 'MH4';
  encrypta         := TextToCriptoHex(encrypta);
  lbSerial.Caption := copy(encrypta, 1, 5) + '-'+
                      copy(encrypta, 6, 5) + '-'+
                      copy(encrypta, 11, 5) + '-'+
                      copy(encrypta, 16, 5);
end;

function TFAtivador.baixarIniSerial: Boolean;
var
  fileDownload : TFileStream;
  sArquivo: String;
  function ExtrairZip(sArquivo: String): Boolean;
  var
    UnZipper: TUnZipper;
  begin
    Result   := False;
    try
      UnZipper := TUnZipper.Create;
      try
        UnZipper.FileName   := sArquivo;
        UnZipper.OutputPath := ExtractFilePath(sArquivo);
        UnZipper.Examine;
        UnZipper.UnZipAllFiles;
        Result := True;
      finally
        UnZipper.Free;
      end;
    except
      Result := False;
    end;
  end;
begin
  Result := False;
  if not(DirectoryExists(ExtractFilePath(Application.ExeName)+'db\')) then
    ForceDirectories(ExtractFilePath(Application.ExeName)+'db\');

  sArquivo := ExtractFilePath(Application.ExeName)+'db\'+sNomeArquivo+'.zip';

  DeleteFile(sArquivo);
  try
    fileDownload := TFileStream.Create(sArquivo, fmCreate);
    try
      dmPrincipal.IdHTTP.Get('http://engrer.com/download/'+sNomeArquivo+'.zip', fileDownload);
    finally
      FreeAndNil(fileDownload);
    end;
  except
    Result := False;
    Exit;
  end;
  if FileExists(sArquivo) then
  begin
    if ExtrairZip(sArquivo) then
    begin
      if FileExists(ExtractFilePath(sArquivo)+sNomeArquivo+'.ini') then
      begin
        DeleteFile(sArquivo);
        Result := True;
      end;
    end;
  end;
end;

function TFAtivador.ChecarLiberacao: Boolean;
var
  ini: TIniFile;
  sArquivo, encrypta, sBaseAtualizada : String;
  Dt_Liberacao, data: TDateTime;
begin
  try
    baixarIniSerial;
    Result        := False;
    sArquivo      := ExtractFilePath(Application.ExeName)+'db\'+sNomeArquivo+'.ini';
    if not(FileExists(sArquivo)) then
    begin
      dmPrincipal.qryTemp.Close;
      dmPrincipal.qryTemp.SQL.Text := 'SELECT COMPARATIVO, BASE, CURRENT_DATE AS DATA_ATUAL FROM configuracoes LIMIT 1';
      dmPrincipal.qryTemp.Open;

      GLOBAL_DATA_COMPARATIVO := CriptoHexToText(dmPrincipal.qryTemp.Fields[0].AsString);
      try
         GLOBAL_DATA_BASE  := StrToDate(CriptoHexToText(dmPrincipal.qryTemp.Fields[1].AsString));
      except
        GLOBAL_DATA_BASE   := Date;
      end;
      GLOBAL_DATA          := dmPrincipal.qryTemp.Fields[2].AsDateTime;
      Result := False;
      Exit;
    end;
    try
      ini := TIniFile.Create(sArquivo);
      Dt_Liberacao := StrToDate(CriptoHexToText(ini.ReadString('SERIAL','CHAVE','31/07/2018')));
    finally
      FreeAndNil(ini);
      DeleteFile(sArquivo);
    end;

    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text := 'SELECT COMPARATIVO, BASE, CURRENT_DATE AS DATA_ATUAL FROM configuracoes LIMIT 1';
    dmPrincipal.qryTemp.Open;

    GLOBAL_DATA_COMPARATIVO := CriptoHexToText(dmPrincipal.qryTemp.Fields[0].AsString);
    try
       GLOBAL_DATA_BASE  := StrToDate(CriptoHexToText(dmPrincipal.qryTemp.Fields[1].AsString));
    except
      GLOBAL_DATA_BASE   := Date;
    end;
    GLOBAL_DATA          := dmPrincipal.qryTemp.Fields[2].AsDateTime;

    if Trim(Global.GLOBAL_DATA_COMPARATIVO) = '' then
      data := Dt_Liberacao
    else
      data := StrToDateTime(Global.GLOBAL_DATA_COMPARATIVO);

    if GLOBAL_DATA_BASE > GLOBAL_DATA then
      Exit;

    if Dt_Liberacao >= data then
    begin
      encrypta         := FormatDateTime('dd/mm/yyyy',Date);
      encrypta         := TextToCriptoHex(encrypta);
      sBaseAtualizada  := copy(encrypta, 1, 5)+
                          copy(encrypta, 6, 5)+
                          copy(encrypta, 11, 5)+
                          copy(encrypta, 16, 5);
      dmPrincipal.qryTemp.Close;
      dmPrincipal.qryTemp.SQL.Text   := ' UPDATE configuracoes SET ' +
                                        ' COMPARATIVO = ' + QuotedStr(TextToCriptoHex(DateToStr(Dt_Liberacao))) + ', ' +
                                        ' BASE = ' + QuotedStr(sBaseAtualizada);
      dmPrincipal.qryTemp.ExecSQL;

      dmPrincipal.qryTemp.Close;
      dmPrincipal.qryTemp.SQL.Text := 'SELECT COMPARATIVO, CURRENT_DATE AS DATA_ATUAL FROM configuracoes LIMIT 1';
      dmPrincipal.qryTemp.Open;

      GLOBAL_DATA_COMPARATIVO := CriptoHexToText(dmPrincipal.qryTemp.Fields[0].AsString);
      GLOBAL_DATA  := dmPrincipal.qryTemp.Fields[1].AsDateTime;

      Result := True;
    end
    else
    begin
      Result := False;
    end;
  except
    try
      dmPrincipal.qryTemp.Close;
      dmPrincipal.qryTemp.SQL.Text := 'SELECT COMPARATIVO, CURRENT_DATE AS DATA_ATUAL FROM configuracoes LIMIT 1';
      dmPrincipal.qryTemp.Open;

      GLOBAL_DATA_COMPARATIVO := CriptoHexToText(dmPrincipal.qryTemp.Fields[0].AsString);
      GLOBAL_DATA             := dmPrincipal.qryTemp.Fields[1].AsDateTime;
    except
      GLOBAL_DATA             := Date;
      dmPrincipal.RodarScript.Clear;
      dmPrincipal.RodarScript.Script.Text := ' ALTER TABLE public.configuracoes '+
                                             '   ADD COLUMN comparativo VARCHAR(100); '+
                                             ' ALTER TABLE public.configuracoes '+
                                             '   ADD COLUMN base VARCHAR(100); ';
      dmPrincipal.RodarScript.Execute;
    end;
    Result := False;
  end;
end;

end.

