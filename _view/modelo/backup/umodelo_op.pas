unit umodelo_op;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, PrintersDlgs, DateTimePicker, Forms, Controls,
  Graphics, Dialogs, ExtCtrls, StdCtrls, Buttons, EditBtn, ufmodelo, db;

type

  { TFModelo_Rel }

  TFModelo_Rel = class(TFModelo)
    btCancelar: TBitBtn;
    btGerar: TBitBtn;
    dsRel: TDataSource;
    Panel1: TPanel;
    procedure btCancelarClick(Sender: TObject);
    procedure btGerarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FModelo_Rel: TFModelo_Rel;

implementation

uses funcoes;

{$R *.lfm}

{ TFModelo_Rel }

procedure TFModelo_Rel.btCancelarClick(Sender: TObject);
begin
  Self.OnCreate(nil);
end;

procedure TFModelo_Rel.btGerarClick(Sender: TObject);
begin

end;

procedure TFModelo_Rel.FormCreate(Sender: TObject);
var
  i : Integer;
begin
  inherited;
  LimpaForm(Self);
  for i := 0 to Self.ComponentCount - 1 do
  begin
    if Self.Components[i] is TDateEdit then
    begin
      (Self.Components[i] as TDateEdit).DateOrder := doDMY;
      if (Self.Components[i] as TDateEdit).name = 'dtInicial' then
        (Self.Components[i] as TDateEdit).Date := PrimeiroDiaMes(Date);
      if (Self.Components[i] as TDateEdit).name = 'dtFinal' then
        (Self.Components[i] as TDateEdit).Date := UltimoDiaMes(Date);
    end;
  end;
end;

end.

