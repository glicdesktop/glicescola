unit ufmodelo;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, eventlog, FileUtil, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, Buttons;

type

  { TFModelo }

  TFModelo = class(TForm)
    imgLogo: TImage;
    btnClose: TImage;
    pnTitulo: TPanel;
    pnPrincipal: TPanel;
    Shape1: TShape;
    procedure btnCloseClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure pnPrincipalClick(Sender: TObject);
    procedure pnTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnTituloMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pnTituloMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    var
      mouseIsDown: Boolean;
      dx:Integer;
      dy:Integer;
  public

  end;

var
  FModelo: TFModelo;

implementation

{$R *.lfm}

{ TFModelo }

procedure TFModelo.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFModelo.FormCreate(Sender: TObject);
begin
 // Self.Left        := 0;
  imgLogo.Hint     := Application.Title;
  pnTitulo.caption := Self.Caption;
end;

procedure TFModelo.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    Close;
end;

procedure TFModelo.FormKeyPress(Sender: TObject; var Key: char);
begin
  if (ActiveControl is TBitBtn) or (ActiveControl is TButton) then
    key:=#0
  else if (key=#13) then
  begin
    key:=#0;
    SelectNext(ActiveControl,true, true);
  end;
end;

procedure TFModelo.pnPrincipalClick(Sender: TObject);
begin
  pnTitulo.caption := Self.Caption;
end;

procedure TFModelo.pnTituloMouseDown(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then begin
    mouseIsDown := True;
    dx := X;
    dy := Y;
  end;
end;

procedure TFModelo.pnTituloMouseMove(Sender: TObject; Shift: TShiftState; X,
  Y: Integer);
begin
  if mouseIsDown then begin
     SetBounds(Self.Left + (X - dx), Self.Top + (Y - dy), Self.Width, Self.Height);
  end;
end;

procedure TFModelo.pnTituloMouseUp(Sender: TObject; Button: TMouseButton;
  Shift: TShiftState; X, Y: Integer);
begin
  mouseIsDown := False;
end;

end.

