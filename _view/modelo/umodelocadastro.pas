unit umodelocadastro;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  EditBtn, ComCtrls, Buttons, DBGrids, DbCtrls, StdCtrls, Menus,
  Mensagem_ctrl;

type

  { TFModeloCadastro }

TFModeloCadastro = class(TForm)
    Bevel1: TBevel;
    Bevel2: TBevel;
    Bevel3: TBevel;
    btCancelar: TSpeedButton;
    btExcluir: TSpeedButton;
    btFiltrar: TBitBtn;
    btRelatorio: TSpeedButton;
    btUtilitario: TSpeedButton;
    btGravar: TSpeedButton;
    btIncluir: TSpeedButton;
    btnClose: TImage;
    btSair: TSpeedButton;
    dsCrud: TDataSource;
    Grid: TDBGrid;
    imgLogo: TImage;
    lbTotal: TLabel;
    menuAcao: TDBNavigator;
    nbMenu: TNotebook;
    Page: TPageControl;
    nbNavegacao: TPage;
    nbEdicao: TPage;
    Panel1: TPanel;
    Panel2: TPanel;
    pnCRUD: TPanel;
    pnFiltro: TPanel;
    pnMenu: TPanel;
    pnPrincipal: TPanel;
    pnTitulo: TPanel;
    pnTituloFiltro: TPanel;
    popupRel: TPopupMenu;
    popupUtil: TPopupMenu;
    Shape1: TShape;
    StatusBar1: TStatusBar;
    TabFicha: TTabSheet;
    TabLista: TTabSheet;
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure btRelatorioClick(Sender: TObject);
    procedure btSairClick(Sender: TObject);
    procedure btUtilitarioClick(Sender: TObject);
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure GridDblClick(Sender: TObject);
    procedure menuAcaoClick(Sender: TObject; Button: TDBNavButtonType);
    procedure pnTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnTituloMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pnTituloMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
    var
      mouseIsDown: Boolean;
      dx:Integer;
      dy:Integer;
  public
    procedure totalRegistros;
  end;

var
  FModeloCadastro: TFModeloCadastro;

implementation

{$R *.lfm}

{ TFModeloCadastro }

procedure TFModeloCadastro.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFModeloCadastro.btRelatorioClick(Sender: TObject);
begin
  popupRel.Popup(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

procedure TFModeloCadastro.btUtilitarioClick(Sender: TObject);
begin
  popupUtil.PopUp(Mouse.CursorPos.X, Mouse.CursorPos.Y);
end;

procedure TFModeloCadastro.btSairClick(Sender: TObject);
begin
  Close;
end;

procedure TFModeloCadastro.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  if dsCrud.DataSet.State in [dsEdit, dsInsert] then
  begin
    btGravar.Enabled    := True;
    btCancelar.Enabled  := True;
    btExcluir.Enabled   := False;
    btIncluir.Enabled   := False;
    menuAcao.Enabled    := False;
    nbMenu.PageIndex    := 1;
    TabLista.TabVisible := False;
  end
  else
  begin
    btGravar.Enabled    := False;
    btCancelar.Enabled  := False;
    btExcluir.Enabled   := True;
    btIncluir.Enabled   := True;
    menuAcao.Enabled    := True;
    nbMenu.PageIndex    := 0;
    TabLista.TabVisible := True;
  end;
end;

procedure TFModeloCadastro.FormCreate(Sender: TObject);
var
  i: Integer;
begin
  Self.Left        := 0;
  imgLogo.Hint     := Application.Title;
  pnTitulo.caption := Self.Caption;
  Page.ActivePage  := TabLista;
  nbMenu.PageIndex := 0;

  if not(popupRel.Items.Menu.Items.Count > 0) then
    btRelatorio.Visible  := False;
  if not(popupUtil.Items.Menu.Items.Count > 0) then
    btUtilitario.Visible := False;

  //for i := 0 to Self.ComponentCount - 1 do
  //begin
  //  if Self.Components[i] is TDateEdit then
  //  begin
  //    (Self.Components[i] as TDateEdit).DateOrder := doDMY;
  //    if (Self.Components[i] as TDateEdit).name = 'dtInicial' then
  //      (Self.Components[i] as TDateEdit).Date := PrimeiroDiaMes(Date);
  //    if (Self.Components[i] as TDateEdit).name = 'dtFinal' then
  //      (Self.Components[i] as TDateEdit).Date := UltimoDiaMes(Date);
  //  end;
  //end;
end;

procedure TFModeloCadastro.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    Close;
end;

procedure TFModeloCadastro.FormKeyPress(Sender: TObject; var Key: char);
begin
  if key=#13 then
   begin
     key:=#0;
     SelectNext(ActiveControl,true, true);
   end;
end;

procedure TFModeloCadastro.GridDblClick(Sender: TObject);
begin
  Page.ActivePage := TabFicha;
end;

procedure TFModeloCadastro.menuAcaoClick(Sender: TObject;
  Button: TDBNavButtonType);
begin
  if Button = nbRefresh then
    btFiltrar.OnClick(nil);
end; 

procedure TFModeloCadastro.pnTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then begin
    mouseIsDown := True;
    dx := X;
    dy := Y;
  end;
end;

procedure TFModeloCadastro.pnTituloMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if mouseIsDown then begin
     SetBounds(Self.Left + (X - dx), Self.Top + (Y - dy), Self.Width, Self.Height);
  end;
end;

procedure TFModeloCadastro.pnTituloMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mouseIsDown := False;
end;

procedure TFModeloCadastro.btExcluirClick(Sender: TObject);
begin
  if (dsCrud.DataSet.IsEmpty) then
    Exit;

  if (TMensagem.GetInstance.Show('Deseja realmente excluir esse registro?', Pergunta_app)) then
  begin
    try
       dsCrud.DataSet.Delete;
    except
      on E : Exception do
      begin
        if e.Message.Contains('violates foreign key constraint') then
          TMensagem.GetInstance.Show('Registro utilizado.'+#13+'Não é possivel remove-lo!',Aviso_app);
      end;
    end;
  end;
  totalRegistros;
end;

procedure TFModeloCadastro.btCancelarClick(Sender: TObject);
begin
  dsCrud.DataSet.Cancel;
  Page.ActivePage := TabLista;
end;

procedure TFModeloCadastro.btGravarClick(Sender: TObject);
var
  i: Integer;
  sObrigatorio: String;
begin
  if Assigned(dsCrud.DataSet.Fields[0]) then
  begin
    try
      dsCrud.DataSet.Fields[0].FocusControl;
    except
    end;
    for i := 0 to pred(dsCrud.DataSet.FieldCount) do
    begin
      if dsCrud.DataSet.Fields[i].Required then
      begin
        if (dsCrud.DataSet.Fields[i].AsString = '') then
          sObrigatorio :=  sObrigatorio + #13 + '* ' + dsCrud.DataSet.Fields[i].DisplayLabel;
      end;
    end;
  end;
  if sObrigatorio <> '' then
  begin
    TMensagem.GetInstance.Show('Campo de preenchimento obrigatório! ' + sObrigatorio, Erro_app);
    Exit;
  end;
  try
    dsCrud.DataSet.Post;
    btFiltrar.OnClick(nil);
    totalRegistros;
    Page.ActivePage := TabLista;
  except
    on E :Exception do
      TMensagem.GetInstance.Show(E.Message,Erro_app);
  end;
end;

procedure TFModeloCadastro.btIncluirClick(Sender: TObject);
begin
  dsCrud.DataSet.Append;
  Page.ActivePage := TabFicha;
end;

procedure TFModeloCadastro.totalRegistros;
begin
  lbTotal.Caption := dsCrud.DataSet.RecordCount.ToString;
end;

end.
