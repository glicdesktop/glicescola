unit USombra;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs;

type

  { TFSombra }

  TFSombra = class(TForm)
    procedure FormCreate(Sender: TObject);
  private

  public

  end;

var
  FSombra: TFSombra;

implementation

{$R *.lfm}

{ TFSombra }

procedure TFSombra.FormCreate(Sender: TObject);
begin
  Self.Align       := alClient;
  Self.WindowState := wsMaximized;
end;

end.
