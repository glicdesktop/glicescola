unit uConta_Gerencial;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro, db;

type

  { TFContaGerencial }

  TFContaGerencial = class(TFModeloCadastro)
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    edPesquisaContaGerencial: TEdit;
    Label1: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    procedure btFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private

  public

  end;

var
  FContaGerencial: TFContaGerencial;

implementation

uses udmPrincipal, Mensagem_Ctrl;

{$R *.lfm}

{ TFContaGerencial }

procedure TFContaGerencial.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  inherited;
  try
    try
      sWhere := '';
      sAnd   := '';

      if (edPesquisaContaGerencial.Text <> '') then
      begin
        sWhere := sWhere + sAnd + ' (nome LIKE ' + QuotedStr(edPesquisaContaGerencial.Text + '%') + ') ';
        sAnd   := ' AND ';
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryContaGerencial.Close;
      dmPrincipal.qryContaGerencial.SQL.Add(sWhere);
      dmPrincipal.qryContaGerencial.SQL.Add(' ORDER BY id ');
      dmPrincipal.qryContaGerencial.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFContaGerencial.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFContaGerencial.TabFichaShow(Sender: TObject);
begin
  DBEdit2.SetFocus;
end;

end.

