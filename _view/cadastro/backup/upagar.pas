unit upagar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DBDateTimePicker, Forms, Controls, Graphics,
  Dialogs, ExtCtrls, ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, DBExtCtrls,
  Menus, EditBtn, umodelocadastro, udmPrincipal, db,
  upagarInclusao;

type

  { TFPagar }

  TFPagar = class(TFModeloCadastro)
    Bevel4: TBevel;
    cbConta_GerencialFiltro: TComboBox;
    DBCheckBox1: TDBCheckBox;
    DBDateTimePicker1: TDBDateTimePicker;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit4: TDBEdit;
    DBGrid1: TDBGrid;
    dsConta_Gerencial: TDataSource;
    dsParcelas: TDataSource;
    dtFinal: TDateEdit;
    dtInicial: TDateEdit;
    edGerencial: TDBEdit;
    edGerencialNome: TDBEdit;
    edPesquisaDescricao: TEdit;
    imgHelp2: TImage;
    Label10: TLabel;
    Label16: TLabel;
    Label19: TLabel;
    Label21: TLabel;
    Label8: TLabel;
    lbTotalGeral: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label6: TLabel;
    lbTotalRegistros: TLabel;
    lbTotalPago: TLabel;
    MenuItem1: TMenuItem;
    MenuItem10: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    MenuItem6: TMenuItem;
    MenuItem7: TMenuItem;
    MenuItem8: TMenuItem;
    MenuItem9: TMenuItem;
    PageControl1: TPageControl;
    Panel3: TPanel;
    popupParcelas: TPopupMenu;
    RadioGroup1: TRadioGroup;
    RadioGroup2: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaVencimento: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaPagamento: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    TabSheet1: TTabSheet;
    procedure btFiltrarClick(Sender: TObject);
    procedure btIncluirClick(Sender: TObject);
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure dsParcelasStateChange(Sender: TObject);
    procedure edGerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure GridDblClick(Sender: TObject);
    procedure MenuItem10Click(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure MenuItem2Click(Sender: TObject);
    procedure MenuItem3Click(Sender: TObject);
    procedure MenuItem4Click(Sender: TObject);
    procedure MenuItem5Click(Sender: TObject);
    procedure MenuItem6Click(Sender: TObject);
    procedure MenuItem7Click(Sender: TObject);
    procedure MenuItem9Click(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private
    procedure getLancamentos;
  public

  end;

var
  FPagar: TFPagar;

implementation

uses funcoes, Acesso_Ctrl, Mensagem_Ctrl, Sombra_Ctrl;

{$R *.lfm}

{ TFPagar }

procedure TFPagar.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(true);
    dmPrincipal.qryContaGerencial.Close;
    dmPrincipal.qryContaGerencial.Open;
    dmPrincipal.qryContaGerencial.First;
    cbConta_GerencialFiltro.Items.Clear;
    cbConta_GerencialFiltro.Items.Add(' ');
    if not(dmPrincipal.qryContaGerencial.IsEmpty) then
    begin
      while not (dmPrincipal.qryContaGerencial.EOF) do
      begin
        try
          cbConta_GerencialFiltro.Items.Add(dmPrincipal.qryContaGerencial.FieldByName('nome').AsString);
        finally
          dmPrincipal.qryContaGerencial.Next;
        end;
      end;
    end;
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(false);
  end;
end;

procedure TFPagar.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  inherited;

  try
    if (dtInicial.Date > dtFinal.Date) or ((dtFinal.Date > 0) and not(dtInicial.Date > 0)) then
    begin
      TMensagem.GetInstance.Show('Período informado é inválido!',Aviso_app);
      Exit;
    end;

    try
      sWhere := '';
      sAnd   := '';

      if (dtInicial.Date > 0) then
      begin
        if rbPesquisaPagamento.Checked then
          sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_pagamento BETWEEN '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtInicial.Date))+' AND '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtFinal.Date))+') '
        else
          sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_vencimento BETWEEN '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtInicial.Date))+' AND '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtFinal.Date))+') ';
        sAND   := ' AND ';
      end;

      if (Trim(cbConta_GerencialFiltro.Text) <> '') then
      begin
        sWhere := sWhere + sAnd + ' (conta_gerencial.nome = ' + QuotedStr(cbConta_GerencialFiltro.Text) + ') ';
        sAnd   := ' AND ';
      end;

      if (Trim(edPesquisaDescricao.Text) <> '') then
      begin
        sWhere := sWhere + sAnd + ' (pagar.descricao LIKE ' + QuotedStr('%'+edPesquisaDescricao.Text+'%') + ') ';
        sAnd   := ' AND ';
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (pagar.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (pagar.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryPagar.Close;
      dmPrincipal.qryPagar.SQL.Add(sWhere);
      dmPrincipal.qryPagar.SQL.Add(' ORDER BY pagar_lancamento.dt_vencimento ');
      dmPrincipal.qryPagar.Open;
      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFPagar.btIncluirClick(Sender: TObject);
var
  FPagarInclusao: TFPagarInclusao;
  Sombra: TSombra;
begin
  inherited;

  //dmPrincipal.qryPagar.Cancel;
  Sombra         := TSombra.Create;
  FPagarInclusao := TFPagarInclusao.Create(Nil);
  try
    FPagarInclusao.ShowModal;
  finally
    FreeAndNil(Sombra);
    FreeAndNil(FPagarInclusao);
    dmPrincipal.qryPagar.Refresh;
  end;
end;

procedure TFPagar.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 46 then
  begin
    if TMensagem.GetInstance.Show('Deseja remover parcela?',Pergunta_app) then
    begin
      dsParcelas.DataSet.Delete;
      getLancamentos;
    end;
  end
  else if Key = 116 then
    getLancamentos;
end;

procedure TFPagar.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  TabFichaShow(Self);
end;

procedure TFPagar.dsParcelasStateChange(Sender: TObject);
begin
  if (dmPrincipal.qryPagarLancamento.State in [dsInsert,dsEdit]) then
  begin
    MenuItem5.Visible := False;
    MenuItem6.Visible := False;
    MenuItem7.Visible := True;
    MenuItem9.Visible := True;
  end
  else
  begin
    MenuItem5.Visible := True;
    MenuItem6.Visible := True;
    MenuItem7.Visible := False;
    MenuItem9.Visible := False;
  end;
end;

procedure TFPagar.edGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  str : String;
begin
  if (Key = 112) and (edGerencial.Enabled) then
  begin
    str := Buscar(conta_gerencial);
    if Trim(str) <> '' then
    begin
      dmPrincipal.qryPagarfk_conta_gerencial.AsInteger := StrToInt(str);
      SelectNext(ActiveControl,true, true);
    end;
  end;
end;

procedure TFPagar.FormCreate(Sender: TObject);
begin
  inherited;
  dtInicial.Date := PrimeiroDiaMes(Date);
  dtFinal.Date   := UltimoDiaMes(Date);
end;

procedure TFPagar.GridDblClick(Sender: TObject);
begin
  Page.ActivePage := TabFicha;
  TabFichaShow(Self);
end;

procedure TFPagar.MenuItem10Click(Sender: TObject);
begin
  getLancamentos;
end;

procedure TFPagar.MenuItem1Click(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(19);
end;

procedure TFPagar.MenuItem2Click(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(20);
end;

procedure TFPagar.MenuItem3Click(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(27);
  getLancamentos;
end;

procedure TFPagar.MenuItem4Click(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(29);
end;

procedure TFPagar.MenuItem5Click(Sender: TObject);
begin
  dsParcelas.DataSet.Last;
  dsParcelas.DataSet.Append;
end;

procedure TFPagar.MenuItem6Click(Sender: TObject);
begin
  dsParcelas.DataSet.Delete;
  getLancamentos;
end;

procedure TFPagar.MenuItem7Click(Sender: TObject);
begin
  dsParcelas.DataSet.Cancel;
end;

procedure TFPagar.MenuItem9Click(Sender: TObject);
begin
  dsParcelas.DataSet.Post;
  getLancamentos;
end;

procedure TFPagar.TabFichaShow(Sender: TObject);
begin
  getLancamentos;
end;

procedure TFPagar.getLancamentos;
var
  rTotalPago, rTotalGeral: Real;
  rTotalRegistro: Integer;
begin
  try
    rTotalPago     := 0;
    rTotalGeral    := 0;
    rTotalRegistro := 0;
    dmPrincipal.qryPagarLancamento.Close;
    if (TabFicha.Showing) then
    begin
      dmPrincipal.qryPagarLancamento.ParamByName('id').AsInteger := dmPrincipal.qryPagarid.AsInteger;
      dmPrincipal.qryPagarLancamento.Open;
      rTotalPago  := 0;
      rTotalGeral := 0;
      while not dmPrincipal.qryPagarLancamento.Eof do
      begin
        rTotalPago  := rTotalPago + dmPrincipal.qryPagarLancamentovl_pago.AsFloat;
        rTotalGeral := rTotalGeral + dmPrincipal.qryPagarLancamentovl.AsFloat;
        dmPrincipal.qryPagarLancamento.Next;
      end;
      rTotalRegistro := dmPrincipal.qryPagarLancamento.RecordCount;
    end;
  finally
    lbTotalRegistros.Caption := 'Registros: ' + rTotalRegistro.ToString;
    lbTotalGeral.Caption     := 'Total geral: ' + FormatFloat('#,0.00', rTotalGeral);
    lbTotalPago.Caption      := 'Total pago: ' + FormatFloat('#,0.00', rTotalPago);
  end;
end;

end.

