unit uConfiguracao;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, ExtCtrls, StdCtrls,
  DBCtrls, ufmodelo, db;

type

  { TFConfiguracao }

  TFConfiguracao = class(TFModelo)
    DBText1: TDBText;
    DBText2: TDBText;
    dsCrud: TDataSource;
    Label1: TLabel;
    Label2: TLabel;
    procedure FormCreate(Sender: TObject);
    procedure pnPrincipalClick(Sender: TObject);
  private

  public

  end;

var
  FConfiguracao: TFConfiguracao;

implementation

{$R *.lfm}

{ TFConfiguracao }

procedure TFConfiguracao.pnPrincipalClick(Sender: TObject);
begin

end;

procedure TFConfiguracao.FormCreate(Sender: TObject);
begin
  inherited;

  dmPrincipal.qryConfiguracao.Close;
  dmPrincipal.qryConfiguracao.Open;
end;

end.

