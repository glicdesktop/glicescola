unit UUsuario;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro, md5,
    db;

type

  { TFUsuario }

  TFUsuario = class(TFModeloCadastro)
    btSenhaNovaAlterar: TButton;
    btSenhaNovaGravar: TButton;
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    edPesquisaNome: TEdit;
    edSenhaNova: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    procedure btCancelarClick(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btSenhaNovaAlterarClick(Sender: TObject);
    procedure btSenhaNovaGravarClick(Sender: TObject);
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  FUsuario: TFUsuario;

implementation

uses Mensagem_Ctrl, udmPrincipal;

{$R *.lfm}

{ TFUsuario }

procedure TFUsuario.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryUsuario.Close;
      dmPrincipal.qryUsuario.SQL.Add(sWhere);
      dmPrincipal.qryUsuario.SQL.Add(' ORDER BY ID ');
      dmPrincipal.qryUsuario.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFUsuario.btGravarClick(Sender: TObject);
begin
  edSenhaNova.Visible        := False;
  btSenhaNovaGravar.Visible  := edSenhaNova.Visible;
  inherited;
end;

procedure TFUsuario.btCancelarClick(Sender: TObject);
begin
  edSenhaNova.Visible        := False;
  btSenhaNovaGravar.Visible  := edSenhaNova.Visible;
  inherited;
end;

procedure TFUsuario.btSenhaNovaAlterarClick(Sender: TObject);
begin
  edSenhaNova.Visible        := not edSenhaNova.Visible;
  btSenhaNovaGravar.Visible  := edSenhaNova.Visible;
  dmPrincipal.qryUsuario.Edit;
end;

procedure TFUsuario.btSenhaNovaGravarClick(Sender: TObject);
var
  sNova: String;
begin
  if (edSenhaNova.Text = '') then
  begin
    TMensagem.GetInstance.Show('Por favor, informe a nova senha.', Erro_app);
  end
  else
  begin
    dmPrincipal.qryUsuario.Edit;
    sNova := MD5Print(MD5String(dmPrincipal.GLOBAL_SALT + edSenhaNova.Text));
    dmPrincipal.qryUsuariosenha.AsString := sNova;
    btSenhaNovaAlterarClick(Self);
  end;
end;

procedure TFUsuario.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  btSenhaNovaAlterar.Enabled := not edSenhaNova.Visible;
  inherited;
end;

procedure TFUsuario.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

end.
