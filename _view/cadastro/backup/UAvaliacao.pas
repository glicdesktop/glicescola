unit UAvaliacao;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro,
    db;

type

  { TFAvaliacao }

  TFAvaliacao = class(TFModeloCadastro)
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  FAvaliacao: TFAvaliacao;

implementation

{$R *.lfm}

{ TFAvaliacao }

procedure TFAvaliacao.FormCreate(Sender: TObject);
begin
  inherited;
end;

procedure TFAvaliacao.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
end;

end.
