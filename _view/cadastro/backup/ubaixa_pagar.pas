unit UBaixa_Pagar;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls, EditBtn, DBGrids, ufmodelo, db;

type

  { TFBaixa_Pagar }

  TFBaixa_Pagar = class(TFModelo)
    btCancelar: TBitBtn;
    btBaixar: TBitBtn;
    btFiltrar: TButton;
    DBGrid1: TDBGrid;
    dsBaixa: TDataSource;
    dtFinal: TDateEdit;
    dtPagamento: TDateEdit;
    dtInicial: TDateEdit;
    edGerencial: TEdit;
    edGerencialNome: TEdit;
    imgHelp2: TImage;
    Label10: TLabel;
    Label11: TLabel;
    Label21: TLabel;
    Label22: TLabel;
    Label26: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lbTotal: TLabel;
    pnCabecalho: TPanel;
    pnRodape: TPanel;
    pnLegenda: TPanel;
    procedure btBaixarClick(Sender: TObject);
    procedure btCancelarClick(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure DBGrid1CellClick(Column: TColumn);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: char);
    procedure edGerencialExit(Sender: TObject);
    procedure edGerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
  private
    iSelecionados : Integer;
  public

  end;

var
  FBaixa_Pagar: TFBaixa_Pagar;

implementation

uses udmPrincipal, funcoes, Global, Mensagem_Ctrl;

{$R *.lfm}

{ TFBaixa_Pagar }

procedure TFBaixa_Pagar.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(True);
    btCancelar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFBaixa_Pagar.btCancelarClick(Sender: TObject);
begin
  iSelecionados    := 0;
  dtPagamento.Date := Date;
  dmPrincipal.tempBaixa_Pagar.Close;
  dmPrincipal.tempBaixa_Pagar.CreateTable;
  dmPrincipal.tempBaixa_Pagar.Open;
  pnCabecalho.Enabled := dmPrincipal.tempBaixa_Pagar.IsEmpty;
  lbTotal.Caption  := 'Total ' + IntToStr(iSelecionados);
  edGerencial.SetFocus;
end;

procedure TFBaixa_Pagar.btBaixarClick(Sender: TObject);
begin
  try
    if not(iSelecionados > 0) then
      Exit;

    if not(TMensagem.GetInstance.Show('Confirma baixa na Data do dia '+dtPagamento.Text+' ?', Pergunta_app)) then
      Exit;

    dmPrincipal.tempBaixa_Pagar.DisableControls;
    dmPrincipal.tempBaixa_Pagar.First;
    while not(dmPrincipal.tempBaixa_Pagar.eof) do
    begin
      if dmPrincipal.tempBaixa_Pagar.FieldByName('baixa').AsBoolean then
      begin
        if not(dmPrincipal.tempBaixa_Pagar.FieldByName('vl_pagamento').AsFloat > 0) then
        begin
          TMensagem.GetInstance.Show('Preencha o valor do pagamento!', Erro_app);
          Exit;
        end;
      end;
      dmPrincipal.tempBaixa_Pagar.Next;
    end;

    dmPrincipal.tempBaixa_Pagar.First;
    while not(dmPrincipal.tempBaixa_Pagar.eof) do
    begin
      try
        if dmPrincipal.tempBaixa_Pagar.FieldByName('baixa').AsBoolean then
        begin
          dmPrincipal.qryTemp.Close;
          dmPrincipal.qryTemp.SQL.Text := ' update pagar_lancamento set '+
                                          ' dt_pagamento = :data, '+
                                          ' vl_pago = :valor '+
                                          ' where pagar_lancamento.id = '+dmPrincipal.tempBaixa_Pagar.FieldByName('id').AsString;
          dmPrincipal.qryTemp.ParamByName('data').AsDateTime := dtPagamento.Date;
          dmPrincipal.qryTemp.ParamByName('valor').AsFloat   := dmPrincipal.tempBaixa_Pagar.FieldByName('vl_pagamento').AsFloat;
          dmPrincipal.qryTemp.ExecSQL;
        end;
      finally
        dmPrincipal.tempBaixa_Pagar.Next;
      end;
    end;
    TMensagem.GetInstance.Show('Baixa realizada com sucesso!', Aviso_app);
    btCancelar.OnClick(nil);
  finally
    dmPrincipal.tempBaixa_Pagar.EnableControls;
  end;
end;

procedure TFBaixa_Pagar.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  inherited;
  try
    if (dtInicial.Date > dtFinal.Date) or ((dtFinal.Date > 0) and not(dtInicial.Date > 0)) then
    begin
      TMensagem.GetInstance.Show('Período informado é inválido!',Aviso_app);
      Exit;
    end;

    try
      sWhere := ' (pagar_lancamento.dt_pagamento is null) ';
      sAnd   := ' AND ';
      if (dtInicial.Date > 0) then
      begin
        sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_vencimento BETWEEN '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtInicial.Date))+' AND '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtFinal.Date))+') ';
        sAND   := ' AND ';
      end;

      if (Trim(edGerencial.Text) <> '') then
      begin
        sWhere := sWhere + sAnd + ' (pagar.fk_conta_gerencial = ' + edGerencial.Text + ') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      btCancelar.OnClick(nil);

      dmPrincipal.qryBaixa_Pagar.Close;
      dmPrincipal.qryBaixa_Pagar.SQL.Add(sWhere);
      dmPrincipal.qryBaixa_Pagar.SQL.Add(' ORDER BY pagar_lancamento.dt_vencimento ');
      dmPrincipal.qryBaixa_Pagar.Open;

      if dmPrincipal.qryBaixa_Pagar.IsEmpty then
      begin
        TMensagem.GetInstance.Show('Nenhum registro encontrado!',Erro_app);
        Exit;
      end;

      dmPrincipal.qryBaixa_Pagar.First;
      while not dmPrincipal.qryBaixa_Pagar.EOF do
      begin
        try
          dmPrincipal.tempBaixa_Pagar.Append;
          dmPrincipal.tempBaixa_Pagar.FieldByName('baixa').AsBoolean          := False;
          dmPrincipal.tempBaixa_Pagar.FieldByName('id').AsInteger             := dmPrincipal.qryBaixa_Pagarid.AsInteger;
          dmPrincipal.tempBaixa_Pagar.FieldByName('descricao').AsString       := dmPrincipal.qryBaixa_Pagardescricao.AsString;
          dmPrincipal.tempBaixa_Pagar.FieldByName('dt_vencimento').AsDateTime := dmPrincipal.qryBaixa_Pagardt_vencimento.AsDateTime;
          dmPrincipal.tempBaixa_Pagar.FieldByName('vl_vencimento').AsFloat    := dmPrincipal.qryBaixa_Pagarvl_vencimento.AsFloat;
          dmPrincipal.tempBaixa_Pagar.FieldByName('vl_pagamento').AsFloat     := dmPrincipal.qryBaixa_Pagarvl_vencimento.AsFloat;
          dmPrincipal.tempBaixa_Pagar.Post;
        finally
          dmPrincipal.qryBaixa_Pagar.Next;
        end;
        dmPrincipal.tempBaixa_Pagar.First;
      end;
    except
    end;
  finally
  end;
end;

procedure TFBaixa_Pagar.DBGrid1CellClick(Column: TColumn);
begin
  if Column = DBGrid1.Columns[0] then
  begin
    if dsBaixa.DataSet.FieldByName('baixa').AsBoolean then
      iSelecionados := iSelecionados - 1
    else
      iSelecionados := iSelecionados + 1;
    dsBaixa.DataSet.Edit;
    dsBaixa.DataSet.FieldByName('baixa').AsBoolean := not(dsBaixa.DataSet.FieldByName('baixa').AsBoolean);
    dsBaixa.DataSet.Post;
    btBaixar.Enabled := (iSelecionados > 0);
    lbTotal.Caption  := 'Total ' + IntToStr(iSelecionados);
  end;
end;

procedure TFBaixa_Pagar.DBGrid1KeyPress(Sender: TObject; var Key: char);
begin
  if not(dsBaixa.DataSet.IsEmpty) then
  begin
    if Key = #42 then
    begin
      if dsBaixa.DataSet.FieldByName('baixa').AsBoolean then
        iSelecionados := iSelecionados - 1
      else
        iSelecionados := iSelecionados + 1;
      dsBaixa.DataSet.Edit;
      dsBaixa.DataSet.FieldByName('baixa').AsBoolean := not(dsBaixa.DataSet.FieldByName('baixa').AsBoolean);
      dsBaixa.DataSet.Post;
    end
    else if Key = #43 then
    begin
      iSelecionados := 0;
      dsBaixa.DataSet.First;
      while not dsBaixa.DataSet.EOF do
      begin
        try
          dsBaixa.DataSet.Edit;
          dsBaixa.DataSet.FieldByName('baixa').AsBoolean := True;
          dsBaixa.DataSet.Post;
        finally
          dsBaixa.DataSet.Next;
          iSelecionados := iSelecionados + 1;
        end;
      end;
    end
    else if Key = #45 then
    begin
      dsBaixa.DataSet.First;
      iSelecionados := 0;
      while not dsBaixa.DataSet.EOF do
      begin
        try
          dsBaixa.DataSet.Edit;
          dsBaixa.DataSet.FieldByName('baixa').AsBoolean := False;
          dsBaixa.DataSet.Post;
        finally
          dsBaixa.DataSet.Next;
        end;
      end;
    end;
    btBaixar.Enabled := (iSelecionados > 0);
    lbTotal.Caption  := 'Total ' + IntToStr(iSelecionados);
  end;
end;

procedure TFBaixa_Pagar.edGerencialExit(Sender: TObject);
begin
  if Trim((Sender as TEdit).Text) = '' then
  begin
    edGerencialNome.Clear;
    Exit;
  end;
  edGerencialNome.Text := getDescricao((Sender as TEdit).Text,conta_gerencial);
  if Trim(edGerencialNome.Text) = '' then
  begin
    TMensagem.GetInstance.Show('Nenhuma conta gerencial encontrada!',Erro_app);
    (Sender as TEdit).Clear;
    (Sender as TEdit).SetFocus;
  end;
end;

procedure TFBaixa_Pagar.edGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if (Key = 112) then
  begin
    (Sender as TEdit).Text := Buscar(conta_gerencial);
    SelectNext(ActiveControl,true, true);
  end;
end;

procedure TFBaixa_Pagar.FormCreate(Sender: TObject);
begin
  inherited;
  iSelecionados  := 0;
  dtInicial.Date := PrimeiroDiaMes(Date);
  dtFinal.Date   := UltimoDiaMes(Date);
end;

procedure TFBaixa_Pagar.FormKeyPress(Sender: TObject; var Key: char);
begin
  if (Key = #13) and (ActiveControl = btFiltrar) then
    btFiltrar.OnClick(nil);
  inherited;
end;

end.

