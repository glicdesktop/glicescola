unit UConteudo;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro,
    db;

type

  { TFConteudo }

  TFConteudo = class(TFModeloCadastro)
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    edPesquisaNome: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label5: TLabel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    procedure btFiltrarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private
  public
  end;

var
  FConteudo: TFConteudo;

implementation

uses Mensagem_Ctrl, udmPrincipal;

{$R *.lfm}

{ TFConteudo }

procedure TFConteudo.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFConteudo.TabFichaShow(Sender: TObject);
begin
  DBEdit2.SetFocus;
end;

procedure TFConteudo.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (conteudo.id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (conteudo.nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (conteudo.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (conteudo.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryConteudo.Close;
      dmPrincipal.qryConteudo.SQL.Add(sWhere);
      dmPrincipal.qryConteudo.SQL.Add(' ORDER BY conteudo.ID ');
      dmPrincipal.qryConteudo.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFConteudo.FormCreate(Sender: TObject);
begin
  inherited;
end;

end.
