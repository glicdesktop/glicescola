unit upagarInclusao;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, BufDataset, SdfData, FileUtil, Forms, Controls,
  Graphics, Dialogs, ExtCtrls, StdCtrls, DbCtrls, DBExtCtrls, DBGrids, Spin,
  EditBtn, Buttons, udmPrincipal, dateutils;

type

  { TFPagarInclusao }

  TFPagarInclusao = class(TForm)
    Bevel1: TBevel;
    btnClose: TImage;
    btGerar: TButton;
    btGravar: TSpeedButton;
    btCancelar: TSpeedButton;
    btExcluir: TButton;
    dsConta_Gerencial: TDataSource;
    dtVencimento: TDateEdit;
    edGerencial: TDBEdit;
    edGerencialNome: TDBEdit;
    edDescricao: TDBEdit;
    edValor_Parcela: TDBEdit;
    DBGrid1: TDBGrid;
    dsCrud: TDataSource;
    dsEmpresa: TDataSource;
    dsFornecedor: TDataSource;
    dsParcelas: TDataSource;
    imgHelp2: TImage;
    imgLogo: TImage;
    Label1: TLabel;
    Label19: TLabel;
    Label2: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    lbTotal: TLabel;
    Panel1: TPanel;
    pnTitulo: TPanel;
    edParcela: TSpinEdit;
    procedure btCancelarClick(Sender: TObject);
    procedure btExcluirClick(Sender: TObject);
    procedure btGerarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure btnCloseClick(Sender: TObject);
    procedure edGerencialKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure pnTituloMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure pnTituloMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Integer);
    procedure pnTituloMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
  private
      mouseIsDown: Boolean;
      dx:Integer;
      dy:Integer;

      procedure totalParcelas;
  public

  end;

var
  FPagarInclusao: TFPagarInclusao;

implementation

uses funcoes, UMsgMensagem, Mensagem_Ctrl;

{$R *.lfm}

{ TFPagarInclusao }

procedure TFPagarInclusao.pnTituloMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  if Button = mbLeft then begin
    mouseIsDown := True;
    dx := X;
    dy := Y;
  end;
end;

procedure TFPagarInclusao.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    Close;
end;

procedure TFPagarInclusao.FormKeyPress(Sender: TObject; var Key: char);
begin
  if key=#13 then
  begin
    key:=#0;
    SelectNext(ActiveControl,true, true);
  end;
end;

procedure TFPagarInclusao.FormShow(Sender: TObject);
begin
  dmPrincipal.qryContaGerencial.Close;
  dmPrincipal.qryContaGerencial.Open;
  edGerencial.SetFocus;
end;

procedure TFPagarInclusao.FormCreate(Sender: TObject);
begin
  imgLogo.Hint      := Application.Title;
  pnTitulo.caption  := Self.Caption;
  dtVencimento.Date := Now;

  dmPrincipal.tempParcelas.Close;
end;

procedure TFPagarInclusao.btGerarClick(Sender: TObject);
var
  i: Integer;
begin
  if Trim(edDescricao.Text) = '' then
  begin
    TMensagem.GetInstance.Show('Informe a Descrição!',Erro_app);
    edDescricao.SetFocus;
    Exit;
  end;

  if Trim(edValor_Parcela.Text) = '' then
  begin
    TMensagem.GetInstance.Show('Informe o Valor da Parcela!',Erro_app);
    edValor_Parcela.SetFocus;
    Exit;
  end;

  if (dmPrincipal.qryPagarvl.AsFloat > 0) and (edParcela.Value > 0) then
  begin
    dmPrincipal.tempParcelas.Close;
    dmPrincipal.tempParcelas.CreateTable;
    dmPrincipal.tempParcelas.Open;
    for i := 1 to edParcela.Value do
    begin
      dmPrincipal.tempParcelas.Append;
      dmPrincipal.tempParcelas.FieldByName('parcela').AsInteger        := i;
      dmPrincipal.tempParcelas.FieldByName('vl').AsFloat               := dmPrincipal.qryPagarvl.AsFloat;
      dmPrincipal.tempParcelas.FieldByName('descricao').AsString       := dmPrincipal.qryPagardescricao.AsString;
      dmPrincipal.tempParcelas.FieldByName('dt_vencimento').AsDateTime := dtVencimento.Date;
      if (i > 1) then
        dmPrincipal.tempParcelas.FieldByName('dt_vencimento').AsDateTime := IncMonth(dtVencimento.Date, i);
      dmPrincipal.tempParcelas.Post;
    end;

    totalParcelas;
    dmPrincipal.tempParcelas.First;
  end;
end;

procedure TFPagarInclusao.btGravarClick(Sender: TObject);
var
  id: Integer;
begin
  if Trim(edGerencialNome.Text) = '' then
  begin
    TMensagem.GetInstance.Show('Informe a Conta Gerencial!',Erro_app);
    edGerencial.SetFocus;
    Exit;
  end;

  if Trim(edDescricao.Text) = '' then
  begin
    TMensagem.GetInstance.Show('Informe a Descrição!',Erro_app);
    edDescricao.SetFocus;
    Exit;
  end;

  if dmPrincipal.tempParcelas.IsEmpty then
  begin
    TMensagem.GetInstance.Show('Nenhuma parcela lançada!',Erro_app);
    edValor_Parcela.SetFocus;
    Exit;
  end;

  id := 0;
  if (dmPrincipal.tempParcelas.RecordCount > 0) then
  begin
    try
      try
        dmPrincipal.qryTemp.Close;
        dmPrincipal.qryTemp.Params.Clear;
        dmPrincipal.qryTemp.SQL.Text := 'SELECT NEXTVAL(''pagar_id_seq'') AS id;';
        dmPrincipal.qryTemp.Open;
        id := dmPrincipal.qryTemp.FieldByName('id').AsInteger;

        dmPrincipal.qryTemp.Close;
        dmPrincipal.qryTemp.SQL.Text := ' INSERT INTO pagar (id, dt_cadastro, vl, id_global, descricao, ativo, fk_conta_gerencial) ' +
                                        ' VALUES (:id, :dt_cadastro, :vl, :id_global, :descricao, :ativo, :fk_conta_gerencial)';
        dmPrincipal.qryTemp.ParamByName('id').AsInteger           := id;
        dmPrincipal.qryTemp.ParamByName('dt_cadastro').AsDateTime := Now;
        dmPrincipal.qryTemp.ParamByName('vl').AsFloat             := dmPrincipal.qryPagarvl.AsFloat;
        dmPrincipal.qryTemp.ParamByName('id_global').AsInteger    := 1;
        dmPrincipal.qryTemp.ParamByName('descricao').AsString     := dmPrincipal.qryPagardescricao.AsString;
        dmPrincipal.qryTemp.ParamByName('ativo').AsString         := 'S';
        dmPrincipal.qryTemp.ParamByName('fk_conta_gerencial').AsInteger := dmPrincipal.qryPagarfk_conta_gerencial.AsInteger;
        dmPrincipal.qryTemp.ExecSQL;

        //dmPrincipal.qryPagarid.AsInteger := id;
        //dmPrincipal.qryPagar.Post;
        //dmPrincipal.qryPagar.ApplyUpdates;

        dmPrincipal.qryPagar.Cancel;
        //id := dmPrincipal.qryPagarid.AsInteger + 1;
        if (id > 0) then
        begin
          dmPrincipal.tempParcelas.First;
          while not dmPrincipal.tempParcelas.EOF do
          begin
            dmPrincipal.qryPagarLancamento.Append;
            dmPrincipal.qryPagarLancamentodt_vencimento.AsDateTime := dmPrincipal.tempParcelas.FieldByName('dt_vencimento').AsDateTime;
            dmPrincipal.qryPagarLancamentofk_pagar.AsInteger       := id;
            dmPrincipal.qryPagarLancamentodescricao.AsString       := dmPrincipal.tempParcelas.FieldByName('descricao').AsString;
            dmPrincipal.qryPagarLancamentovl.AsFloat               := dmPrincipal.tempParcelas.FieldByName('vl').AsFloat;
            dmPrincipal.qryPagarLancamento.Post;

            dmPrincipal.tempParcelas.Next;
          end;

          //dmPrincipal.Conexao.Commit;
        end;

        dmPrincipal.tempParcelas.Close;
        totalParcelas;
        TMensagem.GetInstance.Show('Lançamento realizado com sucesso!', Erro_app);
        Close;
      except
      end;
    finally
    end;
  end;
end;

procedure TFPagarInclusao.btCancelarClick(Sender: TObject);
begin
  Close;
end;

procedure TFPagarInclusao.btExcluirClick(Sender: TObject);
begin
  if (dmPrincipal.tempParcelas.RecordCount > 0) then
  begin
    dmPrincipal.tempParcelas.First;
    while not dmPrincipal.tempParcelas.EOF do
      dmPrincipal.tempParcelas.Delete;
  end;

  totalParcelas;
end;

procedure TFPagarInclusao.btnCloseClick(Sender: TObject);
begin
  Close;
end;

procedure TFPagarInclusao.edGerencialKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  str : String;
begin
  if (Key = 112) and (edGerencial.Enabled) then
  begin
    str := Buscar(conta_gerencial);
    if Trim(str) <> '' then
    begin
      dmPrincipal.qryPagarfk_conta_gerencial.AsInteger := StrToInt(str);
      SelectNext(ActiveControl,true, true);
    end;
  end;
end;

procedure TFPagarInclusao.FormClose(Sender: TObject;
  var CloseAction: TCloseAction);
begin
  if (dmPrincipal.qryPagar.State in [dsInsert]) then
  begin
    dmPrincipal.qryPagar.Cancel;
  end;
end;

procedure TFPagarInclusao.pnTituloMouseMove(Sender: TObject;
  Shift: TShiftState; X, Y: Integer);
begin
  if mouseIsDown then begin
    SetBounds(Self.Left + (X - dx), Self.Top + (Y - dy), Self.Width, Self.Height);
  end;
end;

procedure TFPagarInclusao.pnTituloMouseUp(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
begin
  mouseIsDown := False;
end;

procedure TFPagarInclusao.totalParcelas;
var
  rTotal: Real;
  book: TBookMark;
begin
  rTotal := 0;
  lbTotal.Caption := 'Total: 0';
  if (dmPrincipal.tempParcelas.Active) and (dmPrincipal.tempParcelas.RecordCount > 0) then
  begin
    try
      book := dmPrincipal.tempParcelas.GetBookmark;
      dmPrincipal.tempParcelas.DisableControls;

      dmPrincipal.tempParcelas.First;
      while not dmPrincipal.tempParcelas.EOF do
      begin
        rTotal := rTotal + dmPrincipal.tempParcelas.FieldByName('vl').AsFloat;
        dmPrincipal.tempParcelas.Next;
      end;
    finally
      dmPrincipal.tempParcelas.EnableControls;
      dmPrincipal.tempParcelas.Bookmark := book;

      lbTotal.Caption := FormatFloat('#,0.00', rTotal);
    end;
  end;
end;

end.

