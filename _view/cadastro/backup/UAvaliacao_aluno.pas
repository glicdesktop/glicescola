unit UAvaliacao_aluno;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro,
    db;

type

  { TFAvaliacao_aluno }

  TFAvaliacao_aluno = class(TFModeloCadastro)
    procedure FormClose(Sender: TObject; var CloseAction: TCloseAction);
    procedure FormCreate(Sender: TObject);
  private
  public
  end;

var
  FAvaliacao_aluno: TFAvaliacao_aluno;

implementation

{$R *.lfm}

{ TFAvaliacao_aluno }

procedure TFAvaliacao_aluno.FormCreate(Sender: TObject);
begin
  inherited;
  dao            := TAvaliacao_alunoDAO.Create;
  dsCrud.DataSet := dao.getDataSet();
end;

procedure TFAvaliacao_aluno.FormClose(Sender: TObject; var CloseAction: TCloseAction);
begin
  if Assigned(dao) then
    FreeAndNil(dao);
end;

end.
