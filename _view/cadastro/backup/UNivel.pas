unit UNivel;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro,
    db;

type

  { TFNivel }

  TFNivel = class(TFModeloCadastro)
    Bevel4: TBevel;
    btAddMateria: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBGrid1: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    dsMateria: TDataSource;
    dsNivelMaterias: TDataSource;
    edPesquisaNome: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbTotalRegistros: TLabel;
    PageControl1: TPageControl;
    Panel3: TPanel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    TabSheet1: TTabSheet;
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure GetMateria;
    procedure btAddMateriaClick(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private
  public
  end;

var
  FNivel: TFNivel;

implementation

uses udmPrincipal, Mensagem_Ctrl;

{$R *.lfm}

{ TFNivel }

procedure TFNivel.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (nivel.id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (nivel.nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (nivel.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (nivel.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryNivel.Close;
      dmPrincipal.qryNivel.SQL.Add(sWhere);
      dmPrincipal.qryNivel.SQL.Add(' ORDER BY nivel.ID ');
      dmPrincipal.qryNivel.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFNivel.GetMateria;
var
  rTotalRegistro : Integer;
begin
  try
    dmPrincipal.qryNivelMateria.Close;
    dmPrincipal.qryNivelMateria.ParamByName('id').AsInteger := dmPrincipal.qryNivel.FieldByName('id').AsInteger;
    dmPrincipal.qryNivelMateria.Open;
    rTotalRegistro := dmPrincipal.qryNivelMateria.RecordCount;
  finally
    lbTotalRegistros.Caption := 'Registros: ' + rTotalRegistro.ToString;
  end;
end;

procedure TFNivel.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  inherited;
  GroupBox1.Enabled := (DBEdit1.Text <> '');
  GetMateria;
end;

procedure TFNivel.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  rTotalRegistro : integer;
begin
  if (Key = 46) or (Key = 110) then
  begin
    if dsNivelMaterias.DataSet.IsEmpty then Exit;
    try
      dsNivelMaterias.DataSet.Delete;
      rTotalRegistro := dmPrincipal.qryNivelMateria.RecordCount;
    finally
      lbTotalRegistros.Caption := 'Registros: ' + rTotalRegistro.ToString;
    end;
  end;
end;

procedure TFNivel.btAddMateriaClick(Sender: TObject);
begin
  try
    if not(Trim(DBLookupComboBox1.Text) <> '') then Exit;

    dsNivelMaterias.DataSet.Append;
    dsNivelMaterias.DataSet.FieldByName('fk_nivel').AsInteger   := dsCrud.DataSet.FieldByName('id').AsInteger;
    dsNivelMaterias.DataSet.FieldByName('fk_materia').AsInteger := dmPrincipal.qryMateria.FieldByName('id').AsInteger;
    dsNivelMaterias.DataSet.Post;
  finally
    GetMateria;
  end;
end;

procedure TFNivel.FormShow(Sender: TObject);
begin
  try
    dsMateria.DataSet.Close;
    dmPrincipal.qryMateria.SQL.Add(' order by nome');
    dsMateria.DataSet.Open;
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFNivel.TabFichaShow(Sender: TObject);
begin
  DBEdit2.SetFocus;
  GetMateria;
end;

end.
