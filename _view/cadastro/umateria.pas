unit UMateria;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro;

type

  { TFMateria }

  TFMateria = class(TFModeloCadastro)
    Bevel4: TBevel;
    btAddMateria: TBitBtn;
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBGrid1: TDBGrid;
    DBLookupComboBox1: TDBLookupComboBox;
    dsConteudo: TDataSource;
    dsNivelConteudo: TDataSource;
    edPesquisaNome: TEdit;
    GroupBox1: TGroupBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    lbTotalRegistros: TLabel;
    PageControl1: TPageControl;
    Panel3: TPanel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    TabSheet1: TTabSheet;
    procedure DBGrid1KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure GetConteudo;
    procedure btAddMateriaClick(Sender: TObject);
    procedure btFiltrarClick(Sender: TObject);
    procedure DBGrid1KeyPress(Sender: TObject; var Key: char);
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private

  public

  end;

var
  FMateria: TFMateria;

implementation

uses udmPrincipal, Mensagem_Ctrl;

{$R *.lfm}

{ TFMateria }

procedure TFMateria.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (materia.id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (materia.nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (materia.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (materia.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryMateria.Close;
      dmPrincipal.qryMateria.SQL.Add(sWhere);
      dmPrincipal.qryMateria.SQL.Add(' ORDER BY materia.ID ');
      dmPrincipal.qryMateria.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFMateria.DBGrid1KeyPress(Sender: TObject; var Key: char);
begin

end;

procedure TFMateria.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  GroupBox1.Enabled := (DBEdit1.Text <> '');
  GetConteudo;
  inherited;
end;

procedure TFMateria.FormShow(Sender: TObject);
begin
  dsConteudo.DataSet.Close;
  dmPrincipal.qryConteudo.SQL.Add(' order by nome');
  dsConteudo.DataSet.Open;
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFMateria.TabFichaShow(Sender: TObject);
begin
  DBEdit2.SetFocus;
  GetConteudo;
end;

procedure TFMateria.GetConteudo;
var
  rTotalRegistro : Integer;
begin
  try
    dmPrincipal.qryMateriaConteudo.Close;
    dmPrincipal.qryMateriaConteudo.ParamByName('id').AsInteger := dmPrincipal.qryMateria.FieldByName('id').AsInteger;
    dmPrincipal.qryMateriaConteudo.Open;
    rTotalRegistro := dmPrincipal.qryMateriaConteudo.RecordCount;
  finally
    lbTotalRegistros.Caption := 'Registros: ' + rTotalRegistro.ToString;
  end;
end;

procedure TFMateria.DBGrid1KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  rTotalRegistro : integer;
begin
  if (Key = 46) or (Key = 110) then
  begin
    if dsNivelConteudo.DataSet.IsEmpty then Exit;
    try
      dsNivelConteudo.DataSet.Delete;
      rTotalRegistro := dmPrincipal.qryMateriaConteudo.RecordCount;
    finally
      lbTotalRegistros.Caption := 'Registros: ' + rTotalRegistro.ToString;
    end;
  end;
end;

procedure TFMateria.btAddMateriaClick(Sender: TObject);
begin
  try
    if not(Trim(DBLookupComboBox1.Text) <> '') then Exit;

    dsNivelConteudo.DataSet.Append;
    dsNivelConteudo.DataSet.FieldByName('fk_materia').AsInteger  := dsCrud.DataSet.FieldByName('id').AsInteger;
    dsNivelConteudo.DataSet.FieldByName('fk_conteudo').AsInteger := dsConteudo.DataSet.FieldByName('id').AsInteger;
    dsNivelConteudo.DataSet.Post;
  finally
    GetConteudo;
  end;
end;

end.

