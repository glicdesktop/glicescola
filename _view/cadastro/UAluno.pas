unit UAluno;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, umodelocadastro,
    db;

type

  { TFAluno }

  TFAluno = class(TFModeloCadastro)
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit10: TDBEdit;
    DBEdit11: TDBEdit;
    DBEdit12: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    DBEdit5: TDBEdit;
    DBEdit6: TDBEdit;
    DBEdit7: TDBEdit;
    DBEdit8: TDBEdit;
    DBEdit9: TDBEdit;
    edPesquisaNome: TEdit;
    Label1: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label12: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    procedure btFiltrarClick(Sender: TObject);
    procedure DBEdit10KeyDown(Sender: TObject; var Key: Word; Shift: TShiftState
      );
    procedure FormShow(Sender: TObject);
  private
  public
  end;

var
  FAluno: TFAluno;

implementation

uses udmPrincipal, funcoes, Mensagem_Ctrl;

{$R *.lfm}

{ TFAluno }

procedure TFAluno.FormShow(Sender: TObject);
begin
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFAluno.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (aluno.id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (aluno.nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (aluno.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (aluno.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryAluno.Close;
      dmPrincipal.qryAluno.SQL.Add(sWhere);
      dmPrincipal.qryAluno.SQL.Add(' ORDER BY aluno.id ');
      dmPrincipal.qryAluno.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFAluno.DBEdit10KeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  str : String;
begin
  if (Key = 112) and (DBEdit10.Enabled) then
  begin
    str := Buscar(cidade);
    if Trim(str) <> '' then
    begin
      dmPrincipal.qryAlunofk_cidade.AsInteger := StrToInt(str);
      SelectNext(ActiveControl,true, true);
    end;
  end;
end;

end.
