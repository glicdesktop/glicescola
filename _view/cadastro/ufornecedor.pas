unit ufornecedor;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, RLReport,
  umodelocadastro, db, udmPrincipal, Glic.Mensagem;

type

  { TFFornecedor }

  TFFornecedor = class(TFModeloCadastro)
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBEdit3: TDBEdit;
    DBEdit4: TDBEdit;
    edPesquisaRazaoSocial: TEdit;
    edPesquisaNomeFantasia: TEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    lbDesenvolvedor1: TRLLabel;
    lbTituloRel1: TRLLabel;
    MenuItem1: TMenuItem;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    RLBand2: TRLBand;
    RLBand3: TRLBand;
    RLBand4: TRLBand;
    RLBand5: TRLBand;
    RLBand9: TRLBand;
    RLDBResult1: TRLDBResult;
    RLDBText2: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLLabel1: TRLLabel;
    RLLabel2: TRLLabel;
    RLLabel3: TRLLabel;
    RLLabel7: TRLLabel;
    RLReport1: TRLReport;
    RLSystemInfo2: TRLSystemInfo;
    procedure btFiltrarClick(Sender: TObject);
    procedure DBEdit2Exit(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormKeyPress(Sender: TObject; var Key: char);
    procedure FormShow(Sender: TObject);
    procedure MenuItem1Click(Sender: TObject);
    procedure RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private

  public

  end;

var
  FFornecedor: TFFornecedor;

implementation

uses Global;

{$R *.lfm}

{ TFFornecedor }

procedure TFFornecedor.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin   
  inherited;
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaRazaoSocial.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaRazaoSocial.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (id = ' + edPesquisaRazaoSocial.Text+ ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (razao_social LIKE ' + QuotedStr(edPesquisaRazaoSocial.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (edPesquisaNomeFantasia.Text <> '') then
      begin
        sWhere := sWhere + sAnd + ' (nome_fantasia LIKE ' + QuotedStr(edPesquisaNomeFantasia.Text + '%') + ') ';
        sAnd   := ' AND ';
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryFornecedor.Close;
      dmPrincipal.qryFornecedor.SQL.Add(sWhere);
      dmPrincipal.qryFornecedor.SQL.Add(' ORDER BY nome_fantasia ');
      dmPrincipal.qryFornecedor.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFFornecedor.DBEdit2Exit(Sender: TObject);
begin

end;

procedure TFFornecedor.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  inherited;
end;

procedure TFFornecedor.FormKeyPress(Sender: TObject; var Key: char);
begin
  inherited;
end;

procedure TFFornecedor.FormShow(Sender: TObject);
begin
  try
    TGlicMensagem.GetInstance.Show('', GlicAguarde);
    btFiltrar.OnClick(nil);
  finally
    TGlicMensagem.GetInstance.Show('', GlicAguarde);
  end;
end;

procedure TFFornecedor.MenuItem1Click(Sender: TObject);
begin
  edPesquisaNomeFantasia.Clear;
  edPesquisaRazaoSocial.Clear;
  rbPesquisaAtivo.Checked := True;
  dmPrincipal.qryFornecedor.DisableControls;
  btFiltrar.OnClick(nil);
  if dmPrincipal.qryFornecedor.IsEmpty then
  begin
    TGlicMensagem.GetInstance.Show('Nenhum registro encontrado!',GlicAviso);
    Exit;
  end;
  dmPrincipal.qryFornecedor.IndexFieldNames := 'id';
  RLReport1.Prepare;
  dmPrincipal.qryFornecedor.EnableControls;
  RLReport1.Preview();
end;

procedure TFFornecedor.RLBand2BeforePrint(Sender: TObject; var PrintIt: Boolean
  );
begin
  lbDesenvolvedor1.Caption := Site;
end;

end.

