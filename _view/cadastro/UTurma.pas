unit UTurma;

{$mode objfpc}{$H+}

interface

uses
    Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
    ComCtrls, Buttons, StdCtrls, DBGrids, DbCtrls, Menus, EditBtn,
    umodelocadastro, db;

type

  { TFTurma }

  TFTurma = class(TFModeloCadastro)
    dsNivel: TDataSource;
    DBCheckBox1: TDBCheckBox;
    DBEdit1: TDBEdit;
    DBEdit2: TDBEdit;
    DBLookupComboBox1: TDBLookupComboBox;
    edPesquisaNome: TEdit;
    hrInicio: TTimeEdit;
    hrFim: TTimeEdit;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    RadioGroup1: TRadioGroup;
    rbPesquisaAtivo: TRadioButton;
    rbPesquisaInativo: TRadioButton;
    rbPesquisaTodos: TRadioButton;
    procedure btFiltrarClick(Sender: TObject);
    procedure btGravarClick(Sender: TObject);
    procedure dsCrudDataChange(Sender: TObject; Field: TField);
    procedure FormShow(Sender: TObject);
    procedure TabFichaShow(Sender: TObject);
  private
  public
  end;

var
  FTurma: TFTurma;

implementation

uses udmPrincipal, Mensagem_Ctrl;

{$R *.lfm}

{ TFTurma }

procedure TFTurma.FormShow(Sender: TObject);
begin
  dsNivel.DataSet.Close;
  dsNivel.DataSet.Open;
  try
    TMensagem.GetInstance.Aguarde(True);
    btFiltrar.OnClick(nil);
  finally
    TMensagem.GetInstance.Aguarde(False);
  end;
end;

procedure TFTurma.TabFichaShow(Sender: TObject);
begin
  DBEdit2.SetFocus;
end;

procedure TFTurma.btFiltrarClick(Sender: TObject);
var
  sWhere, sAnd: String;
begin
  try
    try
      sWhere := '';
      sAnd   := '';

      if (Trim(edPesquisaNome.Text) <> '') then
      begin
        try
          if StrToInt(Trim(edPesquisaNome.Text)) > 0 then
          begin
            sWhere := sWhere + sAnd + ' (turma.id = ' + edPesquisaNome.Text + ') ';
            sAnd   := ' AND ';
          end;
        except
          sWhere := sWhere + sAnd + ' (turma.nome LIKE ' + QuotedStr(edPesquisaNome.Text + '%') + ') ';
          sAnd   := ' AND ';
        end;
      end;

      if (rbPesquisaAtivo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (turma.ativo = ''S'') ';
        sAnd   := ' AND ';
      end
      else if (rbPesquisaInativo.Checked) then
      begin
        sWhere := sWhere + sAnd + ' (turma.ativo = ''N'') ';
        sAnd   := ' AND ';
      end;

      if (sWhere <> '') then
        sWhere := ' WHERE ' + sWhere;

      dmPrincipal.qryTurma.Close;
      dmPrincipal.qryTurma.SQL.Add(sWhere);
      dmPrincipal.qryTurma.SQL.Add(' ORDER BY turma.ID ');
      dmPrincipal.qryTurma.Open;

      totalRegistros;
    except
    end;
  finally
  end;
end;

procedure TFTurma.btGravarClick(Sender: TObject);
var
  iDuracao : Double;
begin
  iDuracao := 0;
  if not(hrInicio.Time > 0) then
  begin
    TMensagem.GetInstance.Show('Informe a hora que inicia a aula!',Erro_app);
    Exit;
  end;

  if not(hrFim.Time > 0) then
  begin
    TMensagem.GetInstance.Show('Informe a hora que encerra a aula!',Erro_app);
    Exit;
  end;

  if not(hrFim.Time > hrInicio.Time) then
  begin
    TMensagem.GetInstance.Show('Horário inválido!',Erro_app);
    Exit;
  end;
  iDuracao := hrFim.Time - hrInicio.Time;
  dsCrud.DataSet.FieldByName('horario').AsDateTime := hrInicio.Time;
  dsCrud.DataSet.FieldByName('duracao').Value      := iDuracao;
  inherited;
end;

procedure TFTurma.dsCrudDataChange(Sender: TObject; Field: TField);
begin
  try
    if not (dsCrud.DataSet.State in [dsInsert,dsEdit]) then
    begin
      hrInicio.Time := dsCrud.DataSet.FieldByName('horario').AsDateTime;
      if (dsCrud.DataSet.FieldByName('duracao').AsInteger > 0) and (hrInicio.Time > 0) then
        hrFim.Time := hrInicio.Time + dsCrud.DataSet.FieldByName('duracao').AsInteger;
    end;
  except
  end;
  inherited;
end;

end.
