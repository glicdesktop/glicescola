unit uLogin;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, DividerBevel, Forms, Controls, Graphics, Dialogs,
  ExtCtrls, StdCtrls, udmPrincipal, Mensagem_Ctrl, Acesso_Ctrl, db, md5;

type

  { TFLogin }

  TFLogin = class(TForm)
    btLogar: TButton;
    btFechar: TButton;
    cbLogin: TComboBox;
    DividerBevel1: TDividerBevel;
    edSenha: TEdit;
    Image1: TImage;
    Image2: TImage;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    lbVersao: TLabel;
    Shape1: TShape;
    procedure btFecharClick(Sender: TObject);
    procedure btLogarClick(Sender: TObject);
    procedure cbLoginKeyPress(Sender: TObject; var Key: char);
    procedure edSenhaKeyPress(Sender: TObject; var Key: char);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure LoadOptionsPos;
    procedure SaveOptionsPos;
    var
      iTentativa: Integer;
  public

  end;

var
  FLogin: TFLogin;

implementation

uses jsonConf, Global, Criptografia_app;

{$R *.lfm}

{ TFLogin }

procedure TFLogin.btFecharClick(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(100);
end;

procedure TFLogin.btLogarClick(Sender: TObject);
var
  sCompara: String;
begin
  Perfil_Acesso := '';
  sCompara := MD5Print(MD5String(dmPrincipal.GLOBAL_SALT + edSenha.Text));
  if ((dmPrincipal.qryUsuario.Locate('nome', cbLogin.Text, [loCaseInsensitive])) and (sCompara = dmPrincipal.qryUsuariosenha.Text)) then
  begin
   // Perfil_Acesso := dmPrincipal.qryUsuarioperfil_acesso.AsString;
    dmPrincipal.app.SetUsuario(dmPrincipal.qryUsuarionome.AsString);
    SaveOptionsPos;
    Close;
  end
  else
  begin
    if (iTentativa >= 3) then
      btFecharClick(Self)
    else
    begin
      iTentativa := iTentativa + 1;
      TMensagem.GetInstance.Show('Acesso não permitido!', Erro_app);
    end;
  end;
end;

procedure TFLogin.cbLoginKeyPress(Sender: TObject; var Key: char);
begin
  if key=#13 then
   begin
     key:=#0;
     SelectNext(ActiveControl,true, true);
   end;
end;

procedure TFLogin.edSenhaKeyPress(Sender: TObject; var Key: char);
begin
  if key=#13 then
  begin
    key:=#0;
    btLogar.OnClick(nil);
  end;
end;

procedure TFLogin.FormCreate(Sender: TObject);
var
  encrypta, sBaseAtualizada: String;
begin
  lbVersao.Caption := lbVersao.Caption + ' '+ sVersaoAtual;
  dmPrincipal.qryUsuario.Close;
  dmPrincipal.qryUsuario.SQL.Add(' WHERE ativo IN (''S'') ORDER BY login');
  dmPrincipal.qryUsuario.Open;
  dmPrincipal.qryUsuario.FetchAll;
  while not dmPrincipal.qryUsuario.EOF do
  begin
    cbLogin.Items.Add(dmPrincipal.qryUsuarionome.AsString);
    dmPrincipal.qryUsuario.Next;
  end;
  cbLogin.ItemIndex := -1;

  try
    encrypta         := FormatDateTime('dd/mm/yyyy',Date);
    encrypta         := TextToCriptoHex(encrypta);
    sBaseAtualizada  := copy(encrypta, 1, 5)+
                        copy(encrypta, 6, 5)+
                        copy(encrypta, 11, 5)+
                        copy(encrypta, 16, 5);
    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text   := ' UPDATE configuracoes SET ' +
                                      ' BASE = ' + QuotedStr(sBaseAtualizada);
    dmPrincipal.qryTemp.ExecSQL;
  except
    TAcesso.GetInstance.acessar(100);
    Halt(0);
  end;
end;

procedure TFLogin.FormShow(Sender: TObject);
begin
  iTentativa := 0;
  LoadOptionsPos;
  cbLogin.SetFocus;
  if Trim(cbLogin.Text) <> '' then
    edSenha.SetFocus;
end;

procedure TFLogin.LoadOptionsPos;
var
  nLeft, nTop, nW, nH: Integer;
  c: TJSONConfig;
begin
  if not(FileExists(ExtractFilePath(Application.ExeName) + 'data\')) then
    ForceDirectories(ExtractFilePath(Application.ExeName) + 'data\');

  c := TJSONConfig.Create(Nil);
  try
    c.Filename        := ExtractFilePath(Application.ExeName) + 'data\login.json';
    cbLogin.Text      := c.GetValue('/login/usuario', '');
  finally
    c.Free;
  end;
end;

procedure TFLogin.SaveOptionsPos;
var
  c: TJSONConfig;
begin
  if not(FileExists(ExtractFilePath(Application.ExeName) + 'data\')) then
    ForceDirectories(ExtractFilePath(Application.ExeName) + 'data\');
  c:= TJSONConfig.Create(Nil);
  try
    c.Filename := ExtractFilePath(Application.ExeName) + 'data\login.json';
    c.SetValue('/login/usuario',cbLogin.Text);
  finally
    c.Free;
  end;
end;

end.

