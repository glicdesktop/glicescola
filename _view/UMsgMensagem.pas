unit UMsgMensagem;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TFMsgMensagem }

  TFMsgMensagem = class(TForm)
    btSair: TButton;
    Image1: TImage;
    lbMensagem: TLabel;
    Panel1: TPanel;
    pnPrincipal: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    procedure btSairClick(Sender: TObject);
  private

  public

  end;

var
  FMsgMensagem: TFMsgMensagem;

implementation

{$R *.lfm}

{ TFMsgMensagem }

procedure TFMsgMensagem.btSairClick(Sender: TObject);
begin
  Close;
end;

end.
