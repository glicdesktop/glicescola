unit Upagar_rel;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  Buttons, StdCtrls, EditBtn, RLReport, umodelo_op, db;

type

  { TFContas_Pagar_Rel }

  TFContas_Pagar_Rel = class(TFModelo_Rel)
    cbConta_Gerencial: TComboBox;
    cbEmpresa: TComboBox;
    cbFornecedor: TComboBox;
    dtFinal: TDateEdit;
    dtInicial: TDateEdit;
    Label1: TLabel;
    Label15: TLabel;
    Label2: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    lbDesenvolvedor: TRLLabel;
    lbDesenvolvedor1: TRLLabel;
    lbLojaUsada: TRLLabel;
    lbTituloRel: TRLLabel;
    lbTituloRel1: TRLLabel;
    RLBand1: TRLBand;
    RLBand10: TRLBand;
    RLBand11: TRLBand;
    RLBand12: TRLBand;
    RLBand14: TRLBand;
    RLBand15: TRLBand;
    RLBand16: TRLBand;
    RLBand17: TRLBand;
    RLBand2: TRLBand;
    RLBand3: TRLBand;
    RLBand4: TRLBand;
    RLBand5: TRLBand;
    RLBand6: TRLBand;
    RLBand7: TRLBand;
    RLBand8: TRLBand;
    RLBand9: TRLBand;
    RLDBResult1: TRLDBResult;
    RLDBResult10: TRLDBResult;
    RLDBResult11: TRLDBResult;
    RLDBResult12: TRLDBResult;
    RLDBResult13: TRLDBResult;
    RLDBResult2: TRLDBResult;
    RLDBResult3: TRLDBResult;
    RLDBResult4: TRLDBResult;
    RLDBResult5: TRLDBResult;
    RLDBResult6: TRLDBResult;
    RLDBResult7: TRLDBResult;
    RLDBResult8: TRLDBResult;
    RLDBResult9: TRLDBResult;
    RLDBText1: TRLDBText;
    RLDBText11: TRLDBText;
    RLDBText14: TRLDBText;
    RLDBText15: TRLDBText;
    RLDBText16: TRLDBText;
    RLDBText17: TRLDBText;
    RLDBText2: TRLDBText;
    RLDBText3: TRLDBText;
    RLDBText4: TRLDBText;
    RLDBText5: TRLDBText;
    RLDBText6: TRLDBText;
    RLDBText7: TRLDBText;
    RLDBText8: TRLDBText;
    RLGroup1: TRLGroup;
    RLGroup2: TRLGroup;
    RLGroup3: TRLGroup;
    RLLabel10: TRLLabel;
    RLLabel20: TRLLabel;
    RLLabel21: TRLLabel;
    RLLabel22: TRLLabel;
    RLLabel23: TRLLabel;
    RLLabel4: TRLLabel;
    RLLabel5: TRLLabel;
    RLLabel6: TRLLabel;
    RLLabel7: TRLLabel;
    RLLabel8: TRLLabel;
    RLReport1: TRLReport;
    RLReport2: TRLReport;
    RLSystemInfo1: TRLSystemInfo;
    RLSystemInfo2: TRLSystemInfo;
    procedure btCancelarClick(Sender: TObject);
    procedure btGerarClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure lbDesenvolvedorAfterPrint(Sender: TObject);
    procedure RLBand10BeforePrint(Sender: TObject; var PrintIt: Boolean);
    procedure RLBand4BeforePrint(Sender: TObject; var PrintIt: Boolean);
  private

  public
    bEmAberto : Boolean;
  end;

var
  FContas_Pagar_Rel: TFContas_Pagar_Rel;

implementation

uses Glic.Mensagem, udmPrincipal, Global;

{$R *.lfm}

{ TFContas_Pagar_Rel }

procedure TFContas_Pagar_Rel.btGerarClick(Sender: TObject);
var
  sWHERE, sAND: String;
begin
  if (dtInicial.Date > dtFinal.Date) or ((dtFinal.Date > 0) and not(dtInicial.Date > 0)) then
  begin
    TGlicMensagem.GetInstance.Show('Período informado é inválido!',GlicAviso);
    Exit;
  end;

  sWHERE := '';
  sAND   := '';

  if Self.Tag = 1 then
  begin
    if (dtInicial.Date > 0) then
    begin
      sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_pagamento BETWEEN '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtInicial.Date))+' AND '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtFinal.Date))+') ';
      sAND   := ' AND ';
    end;
  end
  else
  begin
    if (dtInicial.Date > 0) then
    begin
      sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_vencimento BETWEEN '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtInicial.Date))+' AND '+QuotedStr(FormatDateTime('YYYY.MM.DD', dtFinal.Date))+') ';
      sAND   := ' AND ';
    end;
  end;

  if Trim(cbFornecedor.Text) <> '' then
  begin
    sWHERE := sWHERE + sAND + ' (fornecedor.nome_fantasia = '+QuotedStr(cbFornecedor.Text)+') ';
    sAND   := ' AND ';
  end;

  if Trim(cbEmpresa.Text) <> '' then
  begin
    sWHERE := sWHERE + sAND + ' (empresa.nome = '+QuotedStr(cbEmpresa.Text)+') ';
    sAND   := ' AND ';
  end;

  if Trim(cbConta_Gerencial.Text) <> '' then
  begin
    sWHERE := sWHERE + sAND + ' (conta_gerencial.nome = '+QuotedStr(cbConta_Gerencial.Text)+') ';
    sAND   := ' AND ';
  end;

  if bEmAberto then
  begin
    sWHERE := sWHERE + sAND + ' (pagar_lancamento.dt_pagamento is null) ';
    sAND   := ' AND ';
  end;

  if Self.Tag = 1 then
  begin
    sWHERE := sWHERE + sAND + ' not(pagar_lancamento.dt_pagamento is null) ';
    sAND   := ' AND ';
  end;

  if sWHERE <> '' then
    sWHERE := ' WHERE '+ sWHERE;

  with dmPrincipal do
  begin
    qryPagar_Rel.Close;
    qryPagar_Rel.SQL.Add(sWHERE);
    if bEmAberto then
      qryPagar_Rel.SQL.Add(' ORDER BY pagar_lancamento.dt_vencimento, nome_fornecedor, pagar.fk_conta_gerencial ')
    else
      qryPagar_Rel.SQL.Add(' ORDER BY empresa_nome, nome_fornecedor, pagar_lancamento.dt_vencimento, pagar.fk_conta_gerencial ');
    qryPagar_Rel.Open;
    if qryPagar_Rel.IsEmpty then
    begin
      TGlicMensagem.GetInstance.Show('Nenhum registro encontrado!',GlicAviso);
      Exit;
    end;
  end;
  try
    if bEmAberto then
    begin
      if Trim(cbEmpresa.Text) <> '' then
        lbLojaUsada.Caption  := 'Empresa: '+Trim(cbEmpresa.Text)
      else
        lbLojaUsada.Caption  := 'Empresa: Todas';
      lbTituloRel1.Caption := 'Relatório de Contas a Pagar - Em Aberto do dia ' + FormatDateTime('DD/MM/YYYY', dtInicial.Date)+' Até '+FormatDateTime('DD/MM/YYYY', dtFinal.Date);
      RLReport2.Preview();
    end
    else
    begin
      if Self.Tag = 1 then
        lbTituloRel.Caption := 'Relatório de Contas a Pagar - Pagas do dia ' + FormatDateTime('DD/MM/YYYY', dtInicial.Date)+' Até '+FormatDateTime('DD/MM/YYYY', dtFinal.Date)
      else
        lbTituloRel.Caption := 'Relatório de Contas a Pagar do dia ' + FormatDateTime('DD/MM/YYYY', dtInicial.Date)+' Até '+FormatDateTime('DD/MM/YYYY', dtFinal.Date);
      RLReport1.Preview();
    end;
  finally
    FormCreate(Self);
    if bEmAberto then
      pnTitulo.Caption := 'Contas a Pagar - Em Aberto';
  end;
end;

procedure TFContas_Pagar_Rel.btCancelarClick(Sender: TObject);
begin
  inherited;
  if bEmAberto then
    pnTitulo.Caption := 'Contas a Pagar - Em Aberto';
end;

procedure TFContas_Pagar_Rel.FormCreate(Sender: TObject);
begin
  inherited;
end;

procedure TFContas_Pagar_Rel.FormShow(Sender: TObject);
begin
  if bEmAberto then
    pnTitulo.Caption := 'Contas a Pagar - Em Aberto';

  if Self.Tag = 1 then
  begin
    pnTitulo.Caption := 'Contas a Pagar - Pagas';
  end;

  dmPrincipal.qryFornecedor.Close;
  dmPrincipal.qryFornecedor.SQL.Add('order by nome_fantasia');
  dmPrincipal.qryFornecedor.Open;
  dmPrincipal.qryFornecedor.First;
  cbFornecedor.Items.Clear;
  cbFornecedor.Items.Add(' ');
  if not(dmPrincipal.qryFornecedor.IsEmpty) then
  begin
    while not (dmPrincipal.qryFornecedor.EOF) do
    begin
      try
        cbFornecedor.Items.Add(dmPrincipal.qryFornecedor.FieldByName('nome_fantasia').AsString);
      finally
        dmPrincipal.qryFornecedor.Next;
      end;
    end;
  end;

  dmPrincipal.qryConta_Gerencial.Close;
  dmPrincipal.qryConta_Gerencial.SQL.Add('order by nome');
  dmPrincipal.qryConta_Gerencial.Open;
  dmPrincipal.qryConta_Gerencial.First;
  cbConta_Gerencial.Items.Clear;
  cbConta_Gerencial.Items.Add(' ');
  if not(dmPrincipal.qryConta_Gerencial.IsEmpty) then
  begin
    while not (dmPrincipal.qryConta_Gerencial.EOF) do
    begin
      try
        cbConta_Gerencial.Items.Add(dmPrincipal.qryConta_Gerencial.FieldByName('nome').AsString);
      finally
        dmPrincipal.qryConta_Gerencial.Next;
      end;
    end;
  end;

  dmPrincipal.qryEmpresa.Close;
  dmPrincipal.qryEmpresa.SQL.Add('order by nome');
  dmPrincipal.qryEmpresa.Open;
  dmPrincipal.qryEmpresa.First;
  cbEmpresa.Items.Clear;
  cbEmpresa.Items.Add(' ');
  if not(dmPrincipal.qryEmpresa.IsEmpty) then
  begin
    while not (dmPrincipal.qryEmpresa.EOF) do
    begin
      try
        cbEmpresa.Items.Add(dmPrincipal.qryEmpresa.FieldByName('nome').AsString);
      finally
        dmPrincipal.qryEmpresa.Next;
      end;
    end;
  end;
end;

procedure TFContas_Pagar_Rel.lbDesenvolvedorAfterPrint(Sender: TObject);
begin

end;

procedure TFContas_Pagar_Rel.RLBand10BeforePrint(Sender: TObject;
  var PrintIt: Boolean);
begin
  lbDesenvolvedor1.Caption := Site;
end;

procedure TFContas_Pagar_Rel.RLBand4BeforePrint(Sender: TObject; var PrintIt: Boolean
  );
begin
  lbDesenvolvedor.Caption := Site;
end;

end.

