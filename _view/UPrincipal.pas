unit UPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, db, FileUtil, Forms, Controls, Graphics, Dialogs, StdCtrls,
  Arrow, ExtCtrls, DBGrids, Buttons, DbCtrls, ComCtrls, Types;

type

  { TFPrincipal }

  TFPrincipal = class(TForm)
    btAtualizar: TSpeedButton;
    btCliente: TSpeedButton;
    btAvaliacao: TSpeedButton;
    btConfiguracao: TSpeedButton;
    btContaGerencial: TSpeedButton;
    btMateria: TSpeedButton;
    btNivel: TSpeedButton;
    btConteudo: TSpeedButton;
    btEmpresa: TSpeedButton;
    btnClose: TImage;
    btnMinimizar: TImage;
    btPagar: TSpeedButton;
    btUsuario: TSpeedButton;
    Image1: TImage;
    Image2: TImage;
    imgLogo: TImage;
    lbAvisoBloqueio: TLabel;
    lbVersao: TLabel;
    lbUltimoBackup: TLabel;
    lbAtualizacaoDisponivel: TLabel;
    pgMenu: TPageControl;
    pnTitulo: TPanel;
    ShapeBloqueio: TShape;
    ShapeBackup: TShape;
    ShapeAtualizacao: TShape;
    sb: TStatusBar;
    TabPrincipal: TTabSheet;
    TabConfiguracoes: TTabSheet;
    Hora: TTimer;
    procedure btnCloseClick(Sender: TObject);
    procedure btnMinimizarClick(Sender: TObject);
    procedure btUsuarioClick(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure HoraTimer(Sender: TObject);
  private
  public
  end;

var
  FPrincipal: TFPrincipal;

implementation

uses Acesso_Ctrl, udmPrincipal, Global;

{$R *.lfm}

{ TFPrincipal }

procedure TFPrincipal.btnCloseClick(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(100);
end;

procedure TFPrincipal.btnMinimizarClick(Sender: TObject);
begin
  Application.Minimize;
end;

procedure TFPrincipal.btUsuarioClick(Sender: TObject);
begin
  TAcesso.GetInstance.acessar((Sender as TSpeedButton).Tag);
end;

procedure TFPrincipal.FormCreate(Sender: TObject);
begin
  Self.Left        := 0;//-1091;// 0; //-3154;
  pgMenu.PageIndex := 0;
  lbVersao.Caption := Application.Title + ' - ' + lbVersao.Caption + dmPrincipal.app.Versao;
  imgLogo.Hint     := Application.Title;
  lbVersao.Hint    := Application.Title;
  Self.Align       := alClient;
  Self.WindowState := wsMaximized;

  dmPrincipal.qryTemp.Close;
  dmPrincipal.qryTemp.SQL.Text := 'select ultimo_backup from configuracoes limit 1';
  dmPrincipal.qryTemp.open;
  GLOBAL_ULTIMO_BACKUP := dmPrincipal.qryTemp.Fields[0].AsDateTime;
  if GLOBAL_ULTIMO_BACKUP <> Date then
  begin
    TAcesso.GetInstance.FazerBackup;
    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text := 'select ultimo_backup from configuracoes limit 1';
    dmPrincipal.qryTemp.open;
    GLOBAL_ULTIMO_BACKUP := dmPrincipal.qryTemp.Fields[0].AsDateTime;
  end;

  lbUltimoBackup.Caption := 'Último backup Realizado!'+#13+FormatDateTime('DD/MM/YYYY - hh:mm',GLOBAL_ULTIMO_BACKUP);
  lbUltimoBackup.Visible := True;
  ShapeBackup.Visible    := lbUltimoBackup.Visible;

 // TAcesso.GetInstance.ChecarLiberacao;
end;

procedure TFPrincipal.FormShow(Sender: TObject);
begin
  TAcesso.GetInstance.acessar(5);
  //sb.Panels[0].Text := 'Empresa: Cire Box';
  sb.Panels[1].Text := 'Usuário: '+ dmPrincipal.app.Usuario;
end;

procedure TFPrincipal.HoraTimer(Sender: TObject);
begin
  sb.Panels[2].Text := 'Data: '+ DateToStr(Date) + '   Hora: '+TimeToStr(Time);
end;

end.
