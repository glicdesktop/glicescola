unit UMsgPergunta;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, Forms, Controls, Graphics, Dialogs, ExtCtrls,
  StdCtrls;

type

  { TFMsgPergunta }

  TFMsgPergunta = class(TForm)
    btNao: TButton;
    btSim: TButton;
    Image1: TImage;
    lbMensagem: TLabel;
    Panel1: TPanel;
    pnPrincipal: TPanel;
    Shape1: TShape;
    Shape2: TShape;
    Shape3: TShape;
    procedure btNaoClick(Sender: TObject);
    procedure btSimClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
  private

  public

  end;

var
  FMsgPergunta: TFMsgPergunta;

implementation

{$R *.lfm}

{ TFMsgPergunta }

procedure TFMsgPergunta.btSimClick(Sender: TObject);
begin
  ModalResult := mrOk;
end;

procedure TFMsgPergunta.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if Key = 27 then
    btNao.OnClick(nil);
end;

procedure TFMsgPergunta.btNaoClick(Sender: TObject);
begin
  ModalResult := mrNo;
end;

end.
