--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.17
-- Dumped by pg_dump version 9.5.17

-- Started on 2019-07-03 15:18:56 -03

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12525)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 184 (class 1259 OID 16590)
-- Name: aluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.aluno (
    id integer NOT NULL,
    nome character varying(70),
    endereco character varying(40),
    numero character varying(10),
    bairro character varying(30),
    fk_cidade integer,
    cep character varying(9),
    uf character(2),
    cpf character varying(14),
    rg character varying(12),
    complemento character varying(40)
);


ALTER TABLE public.aluno OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 16588)
-- Name: aluno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.aluno_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.aluno_id_seq OWNER TO postgres;

--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 183
-- Name: aluno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.aluno_id_seq OWNED BY public.aluno.id;


--
-- TOC entry 191 (class 1259 OID 16622)
-- Name: avaliacao; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.avaliacao (
    id integer NOT NULL,
    nome character varying(70),
    dt date
);


ALTER TABLE public.avaliacao OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 16630)
-- Name: avaliacao_aluno; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.avaliacao_aluno (
    id integer NOT NULL,
    fk_avaliacao integer,
    fk_turma integer,
    fk_conteudo integer,
    nota integer,
    observacao character varying(200)
);


ALTER TABLE public.avaliacao_aluno OWNER TO postgres;

--
-- TOC entry 192 (class 1259 OID 16628)
-- Name: avaliacao_aluno_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.avaliacao_aluno_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.avaliacao_aluno_id_seq OWNER TO postgres;

--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 192
-- Name: avaliacao_aluno_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.avaliacao_aluno_id_seq OWNED BY public.avaliacao_aluno.id;


--
-- TOC entry 190 (class 1259 OID 16620)
-- Name: avaliacao_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.avaliacao_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.avaliacao_id_seq OWNER TO postgres;

--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 190
-- Name: avaliacao_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.avaliacao_id_seq OWNED BY public.avaliacao.id;


--
-- TOC entry 189 (class 1259 OID 16614)
-- Name: conteudo; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.conteudo (
    id integer NOT NULL,
    nome character varying(100),
    fk_nivel integer,
    material character varying(100),
    ativo character(1) DEFAULT 'S'::bpchar NOT NULL
);


ALTER TABLE public.conteudo OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 16598)
-- Name: nivel; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.nivel (
    id integer NOT NULL,
    nome character varying(20),
    ativo character(1) DEFAULT 'S'::bpchar NOT NULL
);


ALTER TABLE public.nivel OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 16596)
-- Name: nivel_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.nivel_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.nivel_id_seq OWNER TO postgres;

--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 185
-- Name: nivel_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.nivel_id_seq OWNED BY public.nivel.id;


--
-- TOC entry 188 (class 1259 OID 16607)
-- Name: turma; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.turma (
    id integer NOT NULL,
    nome character varying(20),
    horario time with time zone,
    duracao integer,
    ativo character(1) DEFAULT 'S'::bpchar NOT NULL,
    fk_nivel integer
);


ALTER TABLE public.turma OWNER TO postgres;

--
-- TOC entry 187 (class 1259 OID 16605)
-- Name: turma_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.turma_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.turma_id_seq OWNER TO postgres;

--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 187
-- Name: turma_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.turma_id_seq OWNED BY public.turma.id;


--
-- TOC entry 182 (class 1259 OID 16581)
-- Name: usuario; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.usuario (
    id integer NOT NULL,
    nome character varying(70),
    login character varying(70),
    senha character varying(50),
    ativo character(1) DEFAULT 'S'::bpchar NOT NULL
);


ALTER TABLE public.usuario OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 16579)
-- Name: usuario_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.usuario_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.usuario_id_seq OWNER TO postgres;

--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 181
-- Name: usuario_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.usuario_id_seq OWNED BY public.usuario.id;


--
-- TOC entry 2185 (class 2604 OID 16593)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno ALTER COLUMN id SET DEFAULT nextval('public.aluno_id_seq'::regclass);


--
-- TOC entry 2191 (class 2604 OID 16625)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.avaliacao ALTER COLUMN id SET DEFAULT nextval('public.avaliacao_id_seq'::regclass);


--
-- TOC entry 2192 (class 2604 OID 16633)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.avaliacao_aluno ALTER COLUMN id SET DEFAULT nextval('public.avaliacao_aluno_id_seq'::regclass);


--
-- TOC entry 2186 (class 2604 OID 16601)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel ALTER COLUMN id SET DEFAULT nextval('public.nivel_id_seq'::regclass);


--
-- TOC entry 2188 (class 2604 OID 16610)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma ALTER COLUMN id SET DEFAULT nextval('public.turma_id_seq'::regclass);


--
-- TOC entry 2183 (class 2604 OID 16584)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario ALTER COLUMN id SET DEFAULT nextval('public.usuario_id_seq'::regclass);


--
-- TOC entry 2324 (class 0 OID 16590)
-- Dependencies: 184
-- Data for Name: aluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.aluno (id, nome, endereco, numero, bairro, fk_cidade, cep, uf, cpf, rg, complemento) FROM stdin;
\.


--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 183
-- Name: aluno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.aluno_id_seq', 1, false);


--
-- TOC entry 2331 (class 0 OID 16622)
-- Dependencies: 191
-- Data for Name: avaliacao; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.avaliacao (id, nome, dt) FROM stdin;
\.


--
-- TOC entry 2333 (class 0 OID 16630)
-- Dependencies: 193
-- Data for Name: avaliacao_aluno; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.avaliacao_aluno (id, fk_avaliacao, fk_turma, fk_conteudo, nota, observacao) FROM stdin;
\.


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 192
-- Name: avaliacao_aluno_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.avaliacao_aluno_id_seq', 1, false);


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 190
-- Name: avaliacao_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.avaliacao_id_seq', 1, false);


--
-- TOC entry 2329 (class 0 OID 16614)
-- Dependencies: 189
-- Data for Name: conteudo; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.conteudo (id, nome, fk_nivel, material, ativo) FROM stdin;
\.


--
-- TOC entry 2326 (class 0 OID 16598)
-- Dependencies: 186
-- Data for Name: nivel; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.nivel (id, nome, ativo) FROM stdin;
\.


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 185
-- Name: nivel_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.nivel_id_seq', 1, false);


--
-- TOC entry 2328 (class 0 OID 16607)
-- Dependencies: 188
-- Data for Name: turma; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.turma (id, nome, horario, duracao, ativo, fk_nivel) FROM stdin;
\.


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 187
-- Name: turma_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.turma_id_seq', 1, false);


--
-- TOC entry 2322 (class 0 OID 16581)
-- Dependencies: 182
-- Data for Name: usuario; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.usuario (id, nome, login, senha, ativo) FROM stdin;
\.


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 181
-- Name: usuario_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.usuario_id_seq', 1, false);


--
-- TOC entry 2196 (class 2606 OID 16595)
-- Name: aluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.aluno
    ADD CONSTRAINT aluno_pkey PRIMARY KEY (id);


--
-- TOC entry 2206 (class 2606 OID 16635)
-- Name: avaliacao_aluno_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.avaliacao_aluno
    ADD CONSTRAINT avaliacao_aluno_pkey PRIMARY KEY (id);


--
-- TOC entry 2204 (class 2606 OID 16627)
-- Name: avaliacao_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.avaliacao
    ADD CONSTRAINT avaliacao_pkey PRIMARY KEY (id);


--
-- TOC entry 2202 (class 2606 OID 16619)
-- Name: conteudo_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.conteudo
    ADD CONSTRAINT conteudo_pkey PRIMARY KEY (id);


--
-- TOC entry 2198 (class 2606 OID 16604)
-- Name: nivel_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.nivel
    ADD CONSTRAINT nivel_pkey PRIMARY KEY (id);


--
-- TOC entry 2200 (class 2606 OID 16613)
-- Name: turma_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.turma
    ADD CONSTRAINT turma_pkey PRIMARY KEY (id);


--
-- TOC entry 2194 (class 2606 OID 16587)
-- Name: usuario_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id);


--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2019-07-03 15:18:56 -03

--
-- PostgreSQL database dump complete
--

