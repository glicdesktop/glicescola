unit udmPrincipal;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, FileUtil, app_class, IdHTTP, ZConnection, ZDataset,
  ZSqlUpdate, ZSqlProcessor, memds, db;

type

  { TdmPrincipal }

  TdmPrincipal = class(TDataModule)
    Conexao: TZConnection;
    ConexaoMysql: TZConnection;
    IdHTTP: TIdHTTP;
    qryConteudodetalhe: TStringField;
    qryMateriaativo: TStringField;
    qryMateriaConteudoconteudo: TStringField;
    qryMateriaConteudofk_conteudo: TLongintField;
    qryMateriaConteudofk_materia: TLongintField;
    qryMateriaConteudomateria: TStringField;
    qryMaterianome: TStringField;
    qryNivelMateria: TZQuery;
    qryBaixa_Pagar: TZQuery;
    qryBaixa_Pagarconta_gerencial: TStringField;
    qryBaixa_Pagardescricao: TStringField;
    qryBaixa_Pagardt_cadastro: TDateField;
    qryBaixa_Pagardt_pagamento: TDateField;
    qryBaixa_Pagardt_vencimento: TDateField;
    qryBaixa_Pagarfk_conta_gerencial: TLongintField;
    qryBaixa_Pagarid: TLongintField;
    qryBaixa_Pagarid_global: TLongintField;
    qryBaixa_Pagarvl_pago: TFloatField;
    qryBaixa_Pagarvl_vencimento: TFloatField;
    qryCidade: TZQuery;
    qryCidadeid: TLongintField;
    qryCidadeid1: TLongintField;
    qryCidadenome: TStringField;
    qryCidadenome1: TStringField;
    qryCidadeuf: TStringField;
    qryCidadeuf1: TStringField;
    qryConfiguracaobase: TStringField;
    qryConfiguracaocomparativo: TStringField;
    qryConfiguracaoid: TLongintField;
    qryConfiguracaoidcliente: TLongintField;
    qryConfiguracaoultimo_backup: TDateTimeField;
    qryConfiguracaoversao_script: TLongintField;
    qryContaGerencialativo: TStringField;
    qryContaGerencialativo1: TStringField;
    qryContaGerencialnome: TStringField;
    qryContaGerencialnome1: TStringField;
    qryConteudoativo: TStringField;
    qryConteudonome: TStringField;
    qryNivel: TZQuery;
    qryAlunoativo: TStringField;
    qryAlunobairro: TStringField;
    qryAlunocep: TStringField;
    qryAlunocidade: TStringField;
    qryAlunocomplemento: TStringField;
    qryAlunocpf: TStringField;
    qryAlunoendereco: TStringField;
    qryAlunofk_cidade: TLongintField;
    qryAlunonome: TStringField;
    qryAlunonumero: TStringField;
    qryAlunorg: TStringField;
    qryAlunouf: TStringField;
    qryBusca: TZQuery;
    qryBuscaid: TLongintField;
    qryBuscanome: TStringField;
    qryBuscasigla: TStringField;
    qryConteudo: TZQuery;
    qryContaGerencial: TZQuery;
    qryNivelativo: TStringField;
    qryMateria: TZQuery;
    qryMateriaConteudo: TZQuery;
    qryNivelMateriafk_materia: TLongintField;
    qryNivelMateriafk_nivel: TLongintField;
    qryNivelMateriamateria: TStringField;
    qryNivelMaterianivel: TStringField;
    qryNivelnome: TStringField;
    qryPagar: TZQuery;
    qryPagarativo: TStringField;
    qryPagarativo_tela: TStringField;
    qryPagarconta_gerencial: TStringField;
    qryPagardescricao: TStringField;
    qryPagardt_cadastro: TDateField;
    qryPagardt_pagamento: TDateField;
    qryPagardt_vencimento: TDateField;
    qryPagarfk_conta_gerencial: TLongintField;
    qryPagarid: TLongintField;
    qryPagarid_global: TLongintField;
    qryPagarLancamento: TZQuery;
    qryPagarLancamentodescricao: TStringField;
    qryPagarLancamentodt_pagamento: TDateField;
    qryPagarLancamentodt_vencimento: TDateField;
    qryPagarLancamentofk_pagar: TLongintField;
    qryPagarLancamentoid: TLongintField;
    qryPagarLancamentovl: TFloatField;
    qryPagarLancamentovl_pago: TFloatField;
    qryPagarvl: TFloatField;
    qryPagarvl_pago: TFloatField;
    qryPagarvl_vencimento: TFloatField;
    qryPagar_Rel: TZQuery;
    qryPagar_Relconta_gerencial: TStringField;
    qryPagar_Reldescricao: TStringField;
    qryPagar_Reldt_pagamento: TDateField;
    qryPagar_Reldt_vencimento: TDateField;
    qryPagar_Relempresa_nome: TStringField;
    qryPagar_Relfk_conta_gerencial: TLongintField;
    qryPagar_Relfk_empresa: TLongintField;
    qryPagar_Relfk_fornecedor: TLongintField;
    qryPagar_Relfk_pagar: TLongintField;
    qryPagar_Relnome_fornecedor: TStringField;
    qryPagar_Relvl: TFloatField;
    qryPagar_Relvl_pago: TFloatField;
    qryTemp: TZQuery;
    qryTurma: TZQuery;
    qryTurmaid2: TLongintField;
    qryTurmaid3: TLongintField;
    qryTurmaid4: TLongintField;
    qryTurmaid5: TLongintField;
    qryTurmaid6: TLongintField;
    qryTurmaid7: TLongintField;
    qryTurmaid8: TLongintField;
    qryUNivelMateria: TZUpdateSQL;
    qryUNivel: TZUpdateSQL;
    qryUConteudo: TZUpdateSQL;
    qryUContaGerencial: TZUpdateSQL;
    qryUMateria: TZUpdateSQL;
    qryUMateriaConteudo: TZUpdateSQL;
    qryUPagar: TZUpdateSQL;
    qryUPagarLancamento: TZUpdateSQL;
    qryUsuario: TZQuery;
    qryTurmaativo: TStringField;
    qryTurmaduracao: TLongintField;
    qryTurmafk_nivel: TLongintField;
    qryTurmahorario: TField;
    qryTurmaid: TLongintField;
    qryCarregamento_baixa: TZQuery;
    qryScript: TZQuery;
    qryScriptid: TLongintField;
    qryScriptscript: TMemoField;
    qryScriptversao: TLongintField;
    qryTurmaid1: TLongintField;
    qryTurmanivel: TStringField;
    qryTurmanome: TStringField;
    qryAluno: TZQuery;
    qryUsuarioativo: TStringField;
    qryUsuariologin: TStringField;
    qryUsuarionome: TStringField;
    qryUsuariosenha: TStringField;
    qryUTurma: TZUpdateSQL;
    qryUUsuario: TZUpdateSQL;
    qryUAluno: TZUpdateSQL;
    RodarScript: TZSQLProcessor;
    tempBaixa_Pagar: TMemDataset;
    tempCarregamento: TMemDataset;
    tempParcelas: TMemDataset;
    qryConfiguracao: TZQuery;
    qryUConfiguracao: TZUpdateSQL;
    procedure DataModuleCreate(Sender: TObject);
    procedure qryAlunoAfterClose(DataSet: TDataSet);
    procedure qryAlunoAfterInsert(DataSet: TDataSet);
    procedure qryAlunofk_cidadeValidate(Sender: TField);
    procedure qryBaixa_PagarAfterClose(DataSet: TDataSet);
    procedure qryContaGerencialAfterClose(DataSet: TDataSet);
    procedure qryContaGerencialAfterInsert(DataSet: TDataSet);
    procedure qryConteudoAfterClose(DataSet: TDataSet);
    procedure qryConteudoAfterInsert(DataSet: TDataSet);
    procedure qryMateriaAfterClose(DataSet: TDataSet);
    procedure qryMateriaAfterInsert(DataSet: TDataSet);
    procedure qryMateriaConteudoAfterClose(DataSet: TDataSet);
    procedure qryNivelAfterClose(DataSet: TDataSet);
    procedure qryNivelAfterInsert(DataSet: TDataSet);
    procedure qryNivelMateriaAfterClose(DataSet: TDataSet);
    procedure qryPagarAfterClose(DataSet: TDataSet);
    procedure qryPagarAfterInsert(DataSet: TDataSet);
    procedure qryPagarfk_conta_gerencialValidate(Sender: TField);
    procedure qryPagarLancamentoAfterClose(DataSet: TDataSet);
    procedure qryPagarLancamentoAfterInsert(DataSet: TDataSet);
    procedure qryTurmaAfterClose(DataSet: TDataSet);
    procedure qryTurmaAfterInsert(DataSet: TDataSet);
    procedure qryUsuarioAfterClose(DataSet: TDataSet);
    procedure qryUsuarioAfterInsert(DataSet: TDataSet);
  private
    var
      sSqlTurma,
      sSqlUsuario,
      sSqlAluno,
      sSqlNivel,
      sSqlConteudo,
      sSqlPagar,
      sSqlPagar_lancamento,
      sSqlqryBaixa_Pagar,
      sSQLContaGerencial,
      sSqlNivelMateria,
      sSqlMateria,
      sSqlMateriaConteudo,
      sSqlConfiguracao: String;
  public
    app : TAPP;
    GLOBAL_SALT: String;
  end;

var
  dmPrincipal: TdmPrincipal;

implementation

{$R *.lfm}

{ TdmPrincipal }

procedure TdmPrincipal.DataModuleCreate(Sender: TObject);
begin
  try
    GLOBAL_SALT := 'Glic#';
    app := TAPP.Create;

    sSqlTurma    := qryTurma.SQL.Text;
    sSqlUsuario  := qryUsuario.SQL.Text;
    sSqlAluno    := qryAluno.SQL.Text;
    sSqlNivel    := qryNivel.SQL.Text;
    sSqlConteudo := qryConteudo.SQL.Text;
    sSqlPagar    := qryPagar.SQL.Text;
    sSqlPagar_lancamento := qryPagarLancamento.SQL.Text;
    sSQLContaGerencial   := qryContaGerencial.SQL.Text;
    sSqlqryBaixa_Pagar   := qryBaixa_Pagar.SQL.Text;
    sSqlConfiguracao     := qryConfiguracao.SQL.Text;
    sSqlNivelMateria     := qryNivelMateria.SQL.Text;
    sSqlMateria          := qryMateria.SQL.Text;
    sSqlMateriaConteudo  := qryMateriaConteudo.SQL.Text;
  except
  end;
end;

procedure TdmPrincipal.qryAlunoAfterClose(DataSet: TDataSet);
begin
  qryAluno.SQL.Text := sSqlAluno;
end;

procedure TdmPrincipal.qryAlunoAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryAlunofk_cidadeValidate(Sender: TField);
begin
  if Trim(Sender.AsString) <> '' then
  begin
    qryTemp.Close;
    qryTemp.SQL.Text := 'select nome, uf from cidade where id = '+Trim(Sender.AsString);
    qryTemp.Open;
    if not(qryTemp.IsEmpty) then
    begin
      qryAluno.FieldByName('cidade').AsString := qryTemp.FieldByName('nome').AsString;
      qryAluno.FieldByName('uf').AsString     := qryTemp.FieldByName('uf').AsString;
    end
    else
    begin
      qryAluno.FieldByName('cidade').Clear;
      qryAluno.FieldByName('uf').Clear;
    end;
  end;
end;

procedure TdmPrincipal.qryBaixa_PagarAfterClose(DataSet: TDataSet);
begin
  qryBaixa_Pagar.SQL.Text := sSqlqryBaixa_Pagar;
end;

procedure TdmPrincipal.qryContaGerencialAfterClose(DataSet: TDataSet);
begin
  qryContaGerencial.SQL.Text := sSQLContaGerencial;
end;

procedure TdmPrincipal.qryContaGerencialAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryConteudoAfterClose(DataSet: TDataSet);
begin
  qryConteudo.SQL.Text := sSqlConteudo;
end;

procedure TdmPrincipal.qryConteudoAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryMateriaAfterClose(DataSet: TDataSet);
begin
  qryMateria.SQL.Text := sSqlMateria;
end;

procedure TdmPrincipal.qryMateriaAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryMateriaConteudoAfterClose(DataSet: TDataSet);
begin
  qryMateriaConteudo.SQL.Text := sSqlMateriaConteudo;
end;

procedure TdmPrincipal.qryNivelAfterClose(DataSet: TDataSet);
begin
  qryNivel.SQL.Text := sSqlNivel;
end;

procedure TdmPrincipal.qryNivelAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryNivelMateriaAfterClose(DataSet: TDataSet);
begin
  qryNivelMateria.SQL.Text := sSqlNivelMateria;
end;

procedure TdmPrincipal.qryPagarAfterClose(DataSet: TDataSet);
begin
  qryPagar.SQL.Text := sSqlPagar;
end;

procedure TdmPrincipal.qryPagarAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryPagarfk_conta_gerencialValidate(Sender: TField);
begin
  if Trim(Sender.AsString) <> '' then
  begin
    qryTemp.Close;
    qryTemp.SQL.Text := 'select nome from conta_gerencial where id = '+Trim(Sender.AsString);
    qryTemp.Open;
    if not(qryTemp.IsEmpty) then
    begin
      qryPagar.FieldByName('conta_gerencial').AsString := qryTemp.FieldByName('nome').AsString;
    end
    else
    begin
      qryPagar.FieldByName('conta_gerencial').Clear;
    end;
  end;
end;

procedure TdmPrincipal.qryPagarLancamentoAfterClose(DataSet: TDataSet);
begin
  qryPagarLancamento.SQL.Text := sSqlPagar_lancamento;
end;

procedure TdmPrincipal.qryPagarLancamentoAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('fk_pagar').AsInteger := qryPagarid.AsInteger;
end;

procedure TdmPrincipal.qryTurmaAfterClose(DataSet: TDataSet);
begin
  qryTurma.SQL.Text := sSqlTurma;
end;

procedure TdmPrincipal.qryTurmaAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

procedure TdmPrincipal.qryUsuarioAfterClose(DataSet: TDataSet);
begin
  qryUsuario.SQL.Text := sSqlUsuario;
end;

procedure TdmPrincipal.qryUsuarioAfterInsert(DataSet: TDataSet);
begin
  DataSet.FieldByName('ativo').AsString := 'S';
end;

end.
