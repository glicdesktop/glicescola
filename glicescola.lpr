program glicescola;

{$mode objfpc}{$H+}

uses
  {$IFDEF UNIX}{$IFDEF UseCThreads}
  cthreads,
  {$ENDIF}{$ENDIF}
  Interfaces, // this includes the LCL widgetset
  Forms, umodelocadastro, ufmodelo, USombra, UPrincipal, UMsgPergunta,
  UMsgMensagem, UMsgErro, umsgAguarde, uLogin, uBusca, uAtivador, UTurma,
  UNivel, UConteudo, UAvaliacao_aluno, UAvaliacao, UAluno, UUsuario,
  uConta_Gerencial, Sombra_Ctrl, Mensagem_Ctrl, Acesso_Ctrl, pl_indycomp,
  memdslaz, datetimectrls, zcomponent, type_projet, Criptografia_app, app_class,
  udmPrincipal, funcoes, Global, uConfiguracao, upagar, UBaixa_Pagar,
  upagarInclusao, UMateria
  { you can add units after this };

{$R *.res}

begin
  Application.Title:='Glic Escola';
  RequireDerivedFormResource:=True;
  Application.Initialize;
  Application.CreateForm(TdmPrincipal, dmPrincipal);
  Application.CreateForm(TFPrincipal, FPrincipal);
  Application.Run;
end.

