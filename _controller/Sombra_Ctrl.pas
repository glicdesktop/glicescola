unit Sombra_Ctrl;

interface

uses
   USombra;

type
   TSombra = class
   private
      FoForm: TFSombra;
   public
      constructor Create;
      destructor Destroy; override;
   end;

implementation

uses
  SysUtils;

{ TSombra }

constructor TSombra.Create;
begin
   inherited;
   FoForm := TFSombra.Create(nil);
   FoForm.Show;
end;

destructor TSombra.Destroy;
begin
   FoForm.Close;
   FreeAndNil(FoForm);

  inherited;
end;

end.
