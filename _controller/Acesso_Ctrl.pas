unit Acesso_Ctrl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, windows, Forms, type_projet,
  Sombra_Ctrl, Mensagem_Ctrl;

type

  { TAcesso }

  TAcesso = class
  strict private
      class var FInstance : TAcesso;
      constructor createPrivate;
    private
      procedure Sair;
      procedure ShowTurma;
      procedure ShowAluno;
      procedure ShowAvaliacao;
      procedure ShowUsuario;
      procedure ShowLogin;
      procedure ShowNivel;
      procedure ShowContaGerencial;
      procedure ShowConteudo;
      procedure ShowContasPagar;
      procedure ShowBaixaPagar;
      procedure ShowRelPagarBaixados;
      procedure ShowMateria;
      procedure ShowConfiguracao;
      function  ChecarPermissao(Tag: Integer): boolean;
    public
      constructor create;
      procedure FazerBackup;
      function  ChecarLiberacao: Boolean;
      class function GetInstance: TAcesso;
      procedure acessar(Tag: Integer);
    published
    end;

implementation

uses udmPrincipal, UTurma, UAluno, UAvaliacao, UUsuario, UNivel, UConteudo,
  uConta_Gerencial, upagar, UBaixa_Pagar, uLogin, uAtivador, uConfiguracao,
  UMateria;

constructor TAcesso.create;
begin
  raise Exception.Create('Error Message');
end;

procedure TAcesso.FazerBackup;
var
  str: TStringList;
begin
  try
    if not(DirectoryExists(ExtractFilePath(Application.ExeName)+'banco\')) then
      ForceDirectories(ExtractFilePath(Application.ExeName)+'banco\');
    DeleteFile('Backup.bat');
    str := TStringList.Create;
    str.add('set PGUSER=postgres');
    str.add('set PGPASSWORD=guess');
    str.add('');
    str.add('date /t ');
    str.add('set date= ');
    str.add('for /F "tokens=1-3 delims=/ " %%a in (''date /T'') do set date=%%c%%b%%a ');
    str.add('');
    str.add('time /t ');
    str.add('set time= ');
    str.add('for /F "tokens=1-3 delims=: " %%a in (''time /T'') do set time=%%c%%a%%b ');
    str.add('');
    str.add('set date_time= ');
    str.add('set date_time=%date%_%time% ');
    str.add('');
    str.add('cd C:\Program Files (x86)');
    str.add('cd PostgreSQL\10\bin\');
    str.add('pg_dump -U postgres -d glicescola > "C:\lazprojeto\GlicEscola\banco\Backup_%date_time%.sql"');
    str.SaveToFile('Backup.bat');
    WinExec(pChar('Backup.bat'),SW_HIDE);
    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text := 'update configuracoes set ultimo_backup = CURRENT_TIMESTAMP(0);';
    dmPrincipal.qryTemp.ExecSQL;
  finally
    FreeAndNil(str);
    DeleteFile('Backup.bat');
  end;
end;

function TAcesso.ChecarLiberacao: Boolean;
var
  FAtivador : TFAtivador;
begin
  Result    := False;
  FAtivador := TFAtivador.Create(nil);
  try
    if FAtivador.ChecarLiberacao then
      Result := True;
  finally
    FreeAndNil(FAtivador);
  end;
end;

constructor TAcesso.createPrivate;
begin
  inherited create;
end;

procedure TAcesso.acessar(Tag: Integer);
var
  Sombra : TSombra;
begin
  if not ChecarPermissao(Tag) then
  begin
    TMensagem.GetInstance.Show('Usuário não tem permissão de Acesso!',Erro_app);
    Exit;
  end;
  try
    Sombra := TSombra.Create;
    case Tag of
        1  : ShowTurma;
        2  : ShowAluno;
        3  : ShowAvaliacao;
        4  : ShowUsuario;
        5  : ShowLogin;
        6  : ShowContaGerencial;
        9  : ShowNivel;
        10 : ShowConteudo;
        11 : ShowContasPagar;
        27 : ShowBaixaPagar;
        29 : ShowRelPagarBaixados;
        30 : ShowMateria;
        99 : ShowConfiguracao;
      100  : Sair; //Sair do Sistema
      101  : ;     //Tela de Ativação
    end;
  finally
    FreeAndNil(Sombra);
    FreeAndNil(FInstance);
  end;
end;

function TAcesso.ChecarPermissao(Tag: Integer): boolean;
begin
  if dmPrincipal.app.Permissao = Operador then
    Result := False
  else
    Result := True;
end;

class function TAcesso.GetInstance: TAcesso;
begin
  if not Assigned(FInstance) then
    FInstance := TAcesso.createPrivate;
  Result      := FInstance;
end;

procedure TAcesso.Sair;
begin
  Application.Terminate;
end;

procedure TAcesso.ShowTurma;
var
  Tela : TFTurma;
begin
  try
    Tela := TFTurma.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowAluno;
var
  Tela : TFAluno;
begin
  try
    Tela := TFAluno.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowAvaliacao;
var
  Tela : TFAvaliacao;
begin
  try
    Tela := TFAvaliacao.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowUsuario;
var
  Tela : TFUsuario;
begin
  try
    Tela := TFUsuario.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowLogin;
var
  Tela : TFLogin;
begin
  try
    Tela := TFLogin.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowNivel;
var
  Tela : TFNivel;
begin
  try
    Tela := TFNivel.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowContaGerencial;
var
  Tela : TFContaGerencial;
begin
  try
    Tela := TFContaGerencial.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowConteudo;
var
  Tela : TFConteudo;
begin
  try
    Tela := TFConteudo.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowContasPagar;
var
  Tela : TFPagar;
begin
  try
    Tela := TFPagar.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowBaixaPagar;
var
  Tela : TFBaixa_Pagar;
begin
  try
    Tela := TFBaixa_Pagar.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowRelPagarBaixados;
var
  Tela : TFBaixa_Pagar;
begin

end;

procedure TAcesso.ShowMateria;
var
  Tela : TFMateria;
begin
  try
    Tela := TFMateria.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

procedure TAcesso.ShowConfiguracao;
var
  Tela : TFConfiguracao;
begin
  try
    Tela := TFConfiguracao.Create(nil);
    Tela.ShowModal;
  finally
    FreeAndNil(Tela);
  end;
end;

end.
