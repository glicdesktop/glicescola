unit Mensagem_Ctrl;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, UMsgErro, UMsgPergunta,
  UMsgMensagem, Dialogs, LCLType, Controls, umsgAguarde, Sombra_Ctrl;

type

  { TMensagem }

  TMsgDialogo = (Erro_app, Aviso_app, Pergunta_app);

  { TMensagem }

  TMensagem = class
  strict private
      class var FInstance : TMensagem;
      FAguardando: TFAguarde;
      Sombra     : TSombra;
      constructor createPrivate;
    private
      procedure Erro(Mensagem: String; sErro: String = '');
      procedure Aviso(Mensagem: String);
      function  Pergunta(Mensagem: String): Boolean;
    public
      constructor create;
      class function GetInstance: TMensagem;
      procedure Aguarde(Aguardando: Boolean);
      function Show(Mensagem: String; Tipo : TMsgDialogo; sErro: String = ''): Boolean;
    published
    end;

implementation

constructor TMensagem.create;
begin
  raise Exception.Create('Error Message');
end;

constructor TMensagem.createPrivate;
begin
  inherited create;
end;

procedure TMensagem.Erro(Mensagem: String; sErro: String = '');
var
  Tela : TFMsgErro;
begin
  if Assigned(Sombra) then
    FreeAndNil(Sombra);

  Sombra := TSombra.Create;

  Tela := TFMsgErro.Create(nil);
  try
    Tela.lbMensagem.Caption := Mensagem;
    Tela.ShowModal;
  finally
    FreeAndNil(Sombra);
    FreeAndNil(Tela);
  end;
end;

procedure TMensagem.Aviso(Mensagem: String);
var
  Tela : TFMsgMensagem;
begin
  if Assigned(Sombra) then
    FreeAndNil(Sombra);

  Sombra := TSombra.Create;

  Tela   := TFMsgMensagem.Create(nil);
  try
    Tela.lbMensagem.Caption := Mensagem;
    Tela.ShowModal;
  finally
    FreeAndNil(Sombra);
    FreeAndNil(Tela);
  end;
end;

function TMensagem.Pergunta(Mensagem: String): Boolean;
var
  Tela : TFMsgPergunta;
begin
  if Assigned(Sombra) then
    FreeAndNil(Sombra);

  Sombra := TSombra.Create;

  Tela := TFMsgPergunta.Create(nil);
  try
    Tela.lbMensagem.Caption := Mensagem;
    if Tela.ShowModal = mrOK then
      Result := True
    else
      Result := False;
  finally          
    FreeAndNil(Sombra);
    FreeAndNil(Tela);
  end;
end;

class function TMensagem.GetInstance: TMensagem;
begin
  if not Assigned(FInstance) then
    FInstance := TMensagem.createPrivate;
  Result      := FInstance;
end;

procedure TMensagem.Aguarde(Aguardando: Boolean);
begin
  try
    if Aguardando then
    begin
      if Assigned(Sombra) then
        FreeAndNil(Sombra);

      Sombra      := TSombra.Create;

      FAguardando := TFAguarde.Create(nil);
      FAguardando.Show;
      Application.ProcessMessages;
    end
    else
    begin
      if Assigned(FAguardando) then
      begin
        FAguardando.Close;
        FreeAndNil(FAguardando);
        FreeAndNil(Sombra);
      end;
    end;
  finally
  end;
end;

function TMensagem.Show(Mensagem: String; Tipo : TMsgDialogo; sErro: String = ''): Boolean;
begin
  if not (Trim(Mensagem) <> '') then Exit;
  try
    Result := False;
    case Tipo of
      Erro_app     :   // Mensagem de Erro
      begin
        Erro(Mensagem, sErro);
      end;
      Aviso_app    : Aviso(Mensagem);  // Aviso
      Pergunta_app : Result := Pergunta(Mensagem); // Perguntas
    end;
  finally
    FreeAndNil(FInstance);
  end;
end;

end.
