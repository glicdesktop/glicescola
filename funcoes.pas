unit funcoes;

{$mode objfpc}{$H+}

interface

uses
  Classes, Forms, stdctrls, shellapi, process, windows, strutils, SysUtils, Global;

type
  TBuscGlic = (cidade, estado, cliente, fornecedor, motorista, empresa, conta_gerencial, Veiculo);

function verificarplaca(Placa: String): Boolean;
function Buscar(Buscando: TBuscGlic): String;
function PrimeiroDiaMes(Data: TDateTime) : TDateTime;
function UltimoDiaMes(Data: TDateTime) : TDateTime;
function LimpaString(Str: String):String;
function GetDiaSemana(Data: TDateTime):String;
function ExecutarApp(Exe: String): Boolean;
procedure LimpaEdit(Form: TForm);
procedure LimpaCombo(Form: TForm);
procedure LimpaForm(Form: TForm);
function Zerar(Valor: String; Lenght: Integer):String;
function getDescricao(id: String;TipoBusca: TBuscGlic): String;

implementation

uses uBusca, udmPrincipal;

function verificarplaca(Placa: String): Boolean;
var
  sPlaca : String;
begin
  try
    sPlaca := LimpaString(Trim(Placa));

    if sPlaca = '' then
    begin
      Result := False;
      Exit;
    end;

    dmPrincipal.qryTemp.Close;
    dmPrincipal.qryTemp.SQL.Text := 'select id FROM veiculo WHERE veiculo.placa = '+QuotedStr(sPlaca);
    dmPrincipal.qryTemp.Open;

    if not dmPrincipal.qryTemp.IsEmpty then
      Result := True
    else
      Result := False;

  except
    Result := False;
  end;
end;

function Buscar(Buscando: TBuscGlic): String;
var
  FBusca: TFBusca;
begin
  try
    FBusca := TFBusca.Create(nil);
    FBusca.Buscando := Buscando;
    FBusca.ShowModal;
    Result := FBusca.id;
  finally
    FreeAndNil(FBusca);
  end;
end;

function PrimeiroDiaMes(Data: TDateTime) : TDateTime;
var
  ano, mes, dia : word;
  mDtTemp : TDateTime;
begin
  Decodedate(Data, ano, mes, dia);
  mDtTemp := (Data - dia) + 1;
  Result := mDtTemp;
end;

function UltimoDiaMes(Data: TDateTime) : TDateTime;
var
  ano, mes, dia : word;
  mDtTemp : TDateTime;
begin
  Decodedate(Data, ano, mes, dia);
  mDtTemp := (Data - dia) + 33;
  Decodedate(mDtTemp, ano, mes, dia);
  Result := mDtTemp - dia;
end;

function LimpaString(Str: String): String;
var
  Resultado : String;
begin
  Resultado := Trim(Str);
  Resultado := ReplaceStr(Resultado,'.','');
  Resultado := ReplaceStr(Resultado,'/','');
  Resultado := ReplaceStr(Resultado,'*','');
  Resultado := ReplaceStr(Resultado,'-','');
  Resultado := ReplaceStr(Resultado,'\','');
  Resultado := ReplaceStr(Resultado,'+','');
  Resultado := ReplaceStr(Resultado,'#','');
  Resultado := ReplaceStr(Resultado,'_','');
  Resultado := ReplaceStr(Resultado,'(','');
  Resultado := ReplaceStr(Resultado,')','');
  Resultado := ReplaceStr(Resultado,'=','');
  Resultado := ReplaceStr(Resultado,'+','');
  Resultado := ReplaceStr(Resultado,'&','');
  Resultado := ReplaceStr(Resultado,'$','');
  Resultado := ReplaceStr(Resultado,'@','');
  Resultado := ReplaceStr(Resultado,'!','');
  Resultado := ReplaceStr(Resultado,'?','');
  Resultado := ReplaceStr(Resultado,';','');
  Resultado := ReplaceStr(Resultado,':','');
  Result    := Resultado;
end;

function GetDiaSemana(Data: TDateTime): String;
begin
  case DayOfWeek(Data) of
    1: Result := 'Domingo';
    2: Result := 'Segunda-Feira';
    3: Result := 'Terça-Feira';
    4: Result := 'Quarta-Feira';
    5: Result := 'Quinta-Feira';
    6: Result := 'Sexta-Feira';
    7: Result := 'Sábado';
end;
end;

function ExecutarApp(Exe: String): Boolean;
begin
  try
    WinExec(pChar(Exe),SW_NORMAL);
    Result := True;
  except
    Result := False;
  end;
end;

procedure LimpaEdit(Form: TForm);
var
  i: Integer;
begin
  for i := 0 to Form.ComponentCount - 1 do
  begin
    if Form.Components[i] is TCustomEdit then
      (Form.Components[i] as TCustomEdit).Clear;
  end;
end;

procedure LimpaCombo(Form: TForm);
var
  i : Integer;
begin
  for i := 0 to Form.ComponentCount - 1 do
  begin
    if Form.Components[i] is TCustomComboBox then
      (Form.Components[i] as TCustomComboBox).ItemIndex := -1;
  end;
end;

procedure LimpaForm(Form: TForm);
begin
  LimpaEdit(Form);
  LimpaCombo(Form);
end;

function Zerar(Valor: String; Lenght: Integer): String;
var
  i, Tamanho_Atual : Integer;
  Resuldado: String;
begin
  try
    Resuldado     := '';
    Tamanho_Atual := Valor.Length;
    for i := 0 to (Lenght - Tamanho_Atual)-1 do
      Resuldado := '0';
    Resuldado := Resuldado + Valor;
  finally
    Result := Resuldado;
  end;
end;

function getDescricao(id: String; TipoBusca: TBuscGlic): String;
var
  sCampo, sTabela: String;
begin
  if not(Trim(id) <> '') then
  begin
    Result := '';
    Exit;
  end;

  case TipoBusca of
    cliente:
      begin
        sCampo  := 'fantasia';
        sTabela := 'cliente';
      end;
    fornecedor:
      begin
        sCampo  := 'nome_fantasia';
        sTabela := 'fornecedor';
      end;
    motorista:
      begin
        sCampo  := 'nome';
        sTabela := 'motorista';
      end;
    empresa:
      begin
        sCampo  := 'nome';
        sTabela := 'empresa';
      end;
    conta_gerencial:
      begin
        sCampo  := 'nome';
        sTabela := 'conta_gerencial';
      end;
    cidade:
      begin
        sCampo  := 'nome';
        sTabela := 'cidade';
      end;
    estado:
      begin
        sCampo  := 'nome';
        sTabela := 'estado';
      end;
  end;

  if (Trim(sCampo) = '') or (Trim(sTabela) = '') then
  begin
    Result := '';
    Exit;
  end;

  dmPrincipal.qryTemp.Close;
  dmPrincipal.qryTemp.SQL.Text := ' SELECT '+sCampo+' FROM '+sTabela+' where id = '+id;
  dmPrincipal.qryTemp.Open;

  if not dmPrincipal.qryTemp.IsEmpty then
    Result := dmPrincipal.qryTemp.Fields[0].AsString
  else
    Result := '';
end;

end.

