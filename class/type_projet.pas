unit type_projet;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils;

type
  TPermissao = (Admin, Operador);
  TSGDB      = (firebird, mysql, postgres);

function SgdbToStr(SGDB: TSGDB): String;
function StrToSgdb(SGDB: String): TSGDB;


implementation

function SgdbToStr(SGDB: TSGDB): String;
begin
  case SGDB of
    firebird: Result := 'firebird';
    mysql   : Result := 'mysql';
    postgres: Result := 'postgres';
  end;
end;

function StrToSgdb(SGDB: String): TSGDB;
begin
  SGDB := Lowercase(Trim(SGDB));
  if SGDB = 'firebird' then
    Result := firebird
  else if SGDB = 'mysql' then
    Result := mysql
  else if SGDB = 'postgres' then
    Result := postgres
  else
    Result := firebird;
end;

end.

