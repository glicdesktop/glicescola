unit app_class;

{$mode objfpc}{$H+}

interface

uses
  Classes, SysUtils, Forms, type_projet, Dialogs;

type

  { TConexaoConfig }

  TConexaoConfig = class
    strict private
      FSGDB : TSGDB;
      FHost : String;
      FPort : Integer;
      FUser : String;
      FPass : String;
      FBase : String;
    public
      constructor Create;
      function Load : Boolean;
      function Salve: Boolean;
      property SGDB : TSGDB    read FSGDB   write FSGDB;
      property Host : String   read FHost   write FHost;
      property Porta: Integer  read FPort   write FPort;
      property Usuario: String read FUser   write FUser;
      property Senha : String  read FPass   write FPass;
      property Banco : String  read FBase   write FBase;
    end;

  { TAPP }

  TAPP = class
    strict private
      FConexao      : TConexaoConfig;
      FDirectorio   : String;
      FDirSaida     : String;
      FUSUARIO      : String;
      FSTRVERSAO    : String;
      FPERMISSAO    : TPermissao;
    public
      constructor Create;
      property Conexao      : TConexaoConfig read FConexao      write FConexao;
      property Dir          : String         read FDirectorio;
      property DirSaida     : String         read FDirSaida;
      property Usuario      : String         read FUSUARIO;
      property Permissao    : TPermissao     read FPERMISSAO;
      property Versao       : String         read FSTRVERSAO;
      procedure SetUsuario(Value: String);
      procedure SetPermissao(Value: TPermissao); 
  end;


implementation

uses IniFiles, Criptografia_app;

{ TAPP }

constructor TAPP.Create;
begin
  Self.FSTRVERSAO := '1.0.1';
  //if not Assigned(FConexao) then
  //  FConexao  := TConexaoConfig.Create;

  FDirectorio := ExtractFileDir(ApplicationName);
  FDirSaida   := FDirectorio+'data\';

  if not(FileExists(FDirSaida)) then
    ForceDirectories(FDirSaida);
end;

procedure TAPP.SetUsuario(Value: String);
begin
  Self.FUSUARIO := Value;
end;

procedure TAPP.SetPermissao(Value: TPermissao);
begin
  Self.FPERMISSAO := Value;
end; 

{ TConfiguracao }

{ conexao }

constructor TConexaoConfig.Create;
begin
  inherited Create;

  Self.Load;
end;

function TConexaoConfig.Load: Boolean;
var
  ini : TIniFile;
begin
   try
    try
      ini := TIniFile.Create(ExtractFilePath(ApplicationName)+'Config.ini');

      Self.SGDB    := StrToSgdb(CriptoHexToText(ini.ReadString('DB','Tipo','firebird')));
      Self.Host    := CriptoHexToText(ini.ReadString('DB','Host','localhost'));
      Self.Porta   := StrToInt(CriptoHexToText(ini.ReadString('DB','Porta','3050')));
      Self.Banco   := CriptoHexToText(ini.ReadString('DB','Banco','Teste'));
      Self.Usuario := CriptoHexToText(ini.ReadString('DB','Usuario','root'));
      Self.Senha   := CriptoHexToText(ini.ReadString('DB','Senha','123456'));
      Result       := True;
    except
      Result       := False;
    end;
  finally
    FreeAndNil(ini);
  end;
end;

function TConexaoConfig.Salve: Boolean;
var
  ini : TIniFile;
begin
   try
    try
      ini := TIniFile.Create(ExtractFilePath(ApplicationName)+'Config.ini');

      if not(Self.Host.Trim <> '') then
      begin
        ShowMessage('Informe o Host!');
        Result := False;
        Exit;
      end;

      if not (Self.Porta > 0) then
      begin
        ShowMessage('Informe a Porta!');
        Result := False;
        Exit;
      end;

      if not(Self.Usuario.Trim <> '') then
      begin
        ShowMessage('Informe o Usuário!');
        Result := False;
        Exit;
      end;

      ini.WriteString('DB','Tipo',TextToCriptoHex(SgdbToStr(Self.SGDB).Trim()));
      ini.WriteString('DB','Host',TextToCriptoHex(Self.Host.Trim()));
      ini.WriteString('DB','Porta',TextToCriptoHex(Self.Porta.ToString()));
      ini.WriteString('DB','Banco',TextToCriptoHex(Self.Banco.Trim()));
      ini.WriteString('DB','Usuario',TextToCriptoHex(Self.Usuario.Trim()));
      ini.WriteString('DB','Senha',TextToCriptoHex(Self.Senha.Trim()));
      Result       := True;
    except
      Result       := False;
    end;
  finally
    FreeAndNil(ini);
  end;
end;

end.
